<?php
    session_start();
	
    if(!isset($_SESSION["username"]) || $_SESSION['level'] != 'Admin')
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Edit History Keluar";
	
    $username = $_SESSION["username"];
    
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Form Edit History Keluar</h3>
								</div>
								<?php 
									$id = $_GET['id'];
								?>
								<form method="post" action="<?php echo "edit-k.php?id_keluar=".$id; ?>">
									<div class="box-body">
									<?php
									// $id = $_GET['id_masuk'];
									// echo $id;
									$sql = "SELECT * FROM history_keluar where id_keluar='$id'";
    								$res = $helper->database->query($sql);
    								if (!$res) {
    									// echo "Supplier Tidak";
    								} else {
    									$history = $res->fetch_assoc();
    									$tanggal_keluar = $history['tanggal_keluar'];
    									$tgl_klr = $format_date->$tanggal_keluar;
    									
    								}
								?>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="tanggal_pesan">Tanggal Pemesanan</label>
													<input type="text" name="tanggal_pesan" class="form-control" id="tanggal_pesan"  value="<?=$history['tanggal_pesan']?>"  readonly/>
												</div>
												<div class="form-group">
													<label class="control-label" for="tanggal_keluar">Tanggal Kirim</label>
													<input type="text" name="tanggal_keluar" class="form-control" id="tanggal_keluar"  value="<?=$history['tanggal_keluar']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="id_barang">Kode Barang</label>
													<input type="text" name="id_barang" class="form-control" id="id_barang"  value="<?=$history['id_barang']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="jenis_barang">Jenis</label>
													<select name="id_jenis" class="form-control">
														<?php 
														$id_jen = $history['id_jenis'];
														$sql_jenis= "SELECT * FROM jenis_barang";
														$res_jenis = $helper->database->query($sql_jenis);
														if (!$res_jenis) {
															echo "Jenis tidak ada";
														}else{
															if ($res_jenis->num_rows > 0) {
																while ($jenis = $res_jenis->fetch_assoc()) {
																	$id_jenis = $jenis['id_jenis'];
																	$jenis_barang = $jenis['jenis_barang'];
														?>
																	<option value="<?php echo $id_jenis; ?>"<?php if($id_jen==$id_jenis){echo "selected";}else{} ?>><?php echo $jenis_barang; ?></option>
														<?php 
																}//end of while $jenis
																$res_jenis->data_seek(0);
																foreach ($jenis as $jenisnya) {
																		echo $jenisnya['jenis_barang'];
																	}
															}//endif $res_jenis
														}//end of else 
														?>
													</select>
												</div>
												<div class="form-group">
													<label class="control-label" for="ukuran">Ukuran</label>
													<input type="text" name="ukuran" class="form-control" id="ukuran"  value="<?=$history['ukuran']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="keterangan">Keterangan</label>
													<textarea name="keterangan" id="keterangan" class="form-control" cols="20" rows="3"><?=$history['keterangan']?></textarea>
												</div>
												<div class="form-group">
													<label class="control-label" for="jumlah">Jumlah Keluar</label>
													<input type="number" min="0" name="jumlah" class="form-control" id="jumlah"  value="<?=$history['jumlah']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="submit_by">Submit By</label>
													<select name="uplink" class="form-control">
														<?php 
															$uplink = $history['uplink'];
															$sql_uplink= "SELECT * FROM user";
															$res_uplink = $helper->database->query($sql_uplink);
															if (!$res_uplink) {
																echo "Jenis tidak ada";
															}else{
																if ($res_uplink->num_rows > 0) {
																	while ($submit_by = $res_uplink->fetch_assoc()) {
																		// $id_uplink = $submit_by['id_supplier'];
																		$nama_uplink = $submit_by['username'];
														?>
																		<option value="<?php echo $nama_uplink; ?>"<?php if($uplink==$nama_uplink){echo "selected";}else{} ?>><?php echo $nama_uplink; ?></option>
														<?php 
																	}//end of while $res_supplier
																	$res_uplink->data_seek(0);
																}//endif $res_supplier
															}//end of else 
														?>
													</select>
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right"> Update </button>
												</div> 
											</div>
										</div>
									</div>
								</form>								
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/jquery-ui.js"></script>
<script>
var dateKeluar = $("#tanggal_keluar").val();
// alert(dateAwal);
var dateToday = new Date();
var dateOut = $("#tanggal_keluar").datepicker({
	defaultDate: dateKeluar,
	changeMonth: true,
	numberOfMonths: 1,
	minDate: dateToday,
	onSelect: function(selectedDate) {
		var option = this.id == "from" ? "minDate" : "maxDate",
		instance = $(this).data("datepicker"),
		date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
		dateOut.not(this).datepicker("option", option, date);
	}
});
</script>