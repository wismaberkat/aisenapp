<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Profil Saya";
	
    $username = $_SESSION["username"];
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?> <small>Control panel</small></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Ganti password</h3>
								</div>
								<form method="post" action="">
									<div class="box-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="current_password">Password Lama</label>
													<input type="password" name="passlama" class="form-control" id="current_password" value="" required />
												</div>
												<div class="form-group">
													<label class="control-label" for="new_password">Password Baru</label>
													<input type="password" name="passbaru1" class="form-control" id="new_password" required />
												</div>
												<div class="form-group">
													<label class="control-label" for="confirm_password">Konfirmasi Password Baru</label>
													<input type="password" name="passbaru2" class="form-control" id="confirm_password" required />
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right">Change</button>
												</div> 
											</div>
										</div>
									</div>
								</form>
								<?php
									if (isset($_POST['passlama']))
									{
										$password = $helper->database->select("user", "password", "username = '$username'")[0]->password;
										
										$passlama = $_POST["passlama"];
										$passbaru1 = $_POST["passbaru1"];
										$passbaru2 = $_POST["passbaru2"];

										if(!$passlama || !$passbaru1 || !$passbaru2){
											echo "<div class='margin margin-bottom no-print'><div class='alert alert-danger'>Masih Ada Data Yang Kurang</div></div><div class='clearfix'></div>";
											exit();
										}
										else if($password != $passlama){
											echo "<div class='margin margin-bottom no-print'><div class='alert alert-danger'>Password Lama Yang Anda Masukan Salah</div></div><div class='clearfix'></div>";
											exit();
										}
										
										$data = array(
											array("password", $passbaru1)
										);
										
										$result = $helper->database->update("user", $data, "username = '$username'");

										if($result)
											echo "<div class='margin margin-bottom no-print'><div class='alert alert-danger'>Password Anda Berhasil Diubah</div></div><div class='clearfix'></div>";
										else
											echo "<div class='margin margin-bottom no-print'><div class='alert alert-danger'>Ada Kesalahan Pada Saat Mengubah Password</div></div><div class='clearfix'></div>";
									}
								?>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Profil Saya</h3>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label" for="current_password">Nama</label>
												<input type="text" class="form-control" value="<?= $_SESSION["nama"]; ?>" readonly />
											</div>
											<div class="form-group">
												<label class="control-label" for="current_password">Username</label>
												<input type="text" class="form-control" value="<?= $_SESSION["username"]; ?>" readonly />
											</div>
											<div class="form-group">
												<label class="control-label" for="current_password">Telepone</label>
												<input type="text" class="form-control" value="<?= $_SESSION["hp"]; ?>" readonly />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>