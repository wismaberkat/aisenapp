<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
    	
    $username = $_SESSION["username"];
	$pageTitle = "Daftar Barang";

?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <!-- <section class="content eror"> -->
                    <?php 
                    if (isset($_GET['act']) && isset($_GET['id_barang']) && $_SESSION['level'] == 'Admin') {
                        $sql = "DELETE from daftar_barang where id_barang = '".$_GET['id_barang']."'";
                        $resdel = $helper->database->query($sql);
                        if ($resdel) {
                            $idDeleted = $_GET['id_barang'];
                            echo '<div class="box-body">
                            <div class="alert alert-success alert-dismissible nomarbot" style="display:block;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Sukses !</h4>
                            Kode Barang'.$idDeleted.' Berhasil di hapus.
                            </div>
                            </div>';
                        } else {
                            $idUpdated = $_GET['id'];
                            echo '<div class="box-body">
                            <div class="alert alert-danger alert-dismissible nomarbot" style="display:block;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-close"></i> Gagal !</h4>
                            Kode Barang '.$idUpdated.' Gagal di hapus.
                            </div>
                            </div>';
                        }
                    }
                    if(isset($_GET['isUpdate']) && isset($_GET['id']))
                    {   
                        $isUpdate = $_GET['isUpdate'];
                        if ($isUpdate==1) {
                            $idUpdated = $_GET['id'];
                            echo '<div class="box-body">
                            <div class="alert alert-success alert-dismissible nomarbot" style="display:block;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Sukses !</h4>
                            Kode Barang '.$idUpdated.' Berhasil diupdate.
                            </div>
                            </div>';
                        } else {
                            $idUpdated = $_GET['id'];
                            echo '<div class="box-body">
                            <div class="alert alert-danger alert-dismissible nomarbot" style="display:block;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-close"></i> Gagal !</h4>
                            Kode Barang '.$idUpdated.' Gagal diupdate.
                            </div>
                            </div>';
                        }
                    }
                    ?>
                <!-- </section> -->
                <section class="content">
                    <div class="block">
                        <div class="block">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tabel Daftar Barang</h3>
                                    <form action="hasil_search_barang.php" method="post">
                                        <div class="col-md-4" style="float:right;">
                                            <div class="input-group">
                                                <input type="text" name="keyword" class="form-control" placeholder="Masukkan Kode Barang/Jenis Barang" required>
                                                <span class="input-group-btn">
                                                    <input type="submit" class="btn btn-default" value="Cari">
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                    <th class="rata-tengah">NO</th>
                                                    <th class="rata-tengah">Kode Barang</th>
                                                    <th class="rata-tengah">Jenis Barang</th>
                                                    <th class="rata-tengah">Ukuran</th>
                                                    <th class="rata-tengah">Foto</th>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <th class="rata-tengah">BP</th>
                                                    <?php endif; ?>
                                                    <th class="rata-tengah">Harga Jual</th>
                                                    <th class="rata-tengah">Supplier</th>
                                                    <th class="rata-tengah">Keterangan</th>
                                                    <th class="rata-tengah">Stok</th>
                                                    <th class="rata-tengah">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
													$i = 1;
                                                    // $condition = "ORDER BY id_barang DESC";
                                                    $barang = $helper->database->select("daftar_barang", "*","1","id_barang DESC");
                                                    if (!$barang) {
                                                        echo "tidak ada stok.";
                                                    } else {
    													foreach($barang as $stock_barang)
    													{
                                                            //get jenis_barang
                                                            $id_jenis = $stock_barang->id_jenis;
                                                            $j_condition = "id_jenis='$id_jenis'";
                                                            $jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);
                                                            // echo $jenis;

                                                            $html = "<tr>";
                                                            $html .= "<td>".$i."</td>";
                                                            $html .= "<td align='center'>".$stock_barang->id_barang."</td>";

                                                            foreach($jenis as $jenis_barang)
                                                            {
                                                                $html .= "<td align='center'>".$jenis_barang->jenis_barang."</td>";
                                                            }
                                                            $html .= "<td align='center'>".$stock_barang->ukuran."</td>";
                                                            $foto = "<img src='data:image/jpeg;base64,".base64_encode($stock_barang->foto)."' height='370' width='470'/>";
                                                            $html .= "<td align='center'>".$foto."</td>";
                                                            // $html .= "<td align='center'>".base64_encode($stock_barang->foto)."</td>";
    														// $html .= "<td align='center'><img src='data:image/jpeg;base64,".base64_encode($stock_barang->foto)."' height='370' width='470'/></td>";

                                                            // echo '<img src="data:image/jpeg;base64,'.base64_encode($result['nama_file']).'" height = "370" width = "470"/>';
                                                            
                                                            if($_SESSION['level'] == 'Admin'):
                                                            $html .= "<td align='right'>".$helper->format_rupiah($stock_barang->h_modal)."</td>";
                                                            endif;
                                                            $html .= "<td align='right'>".$helper->format_rupiah($stock_barang->h_jual)."</td>";

                                                            //get nama_supplier
                                                            $id_supplier = $stock_barang->id_supplier;
                                                            $s_condition = "id_supplier='$id_supplier'";
                                                            $supp = $helper->database->select("supplier", "nama_supplier",$s_condition);
                                                            foreach($supp as $supplier)
                                                            {
                                                                $html .= "<td align='center'>".$supplier->nama_supplier."</td>";
                                                            }
                                                            $html .= "<td align='center'>".$stock_barang->keterangan."</td>";
                                                            $html .= "<td align='right'>".$stock_barang->jumlah."</td>";
                                                            $html .= "<td align='center'><a href=\"".$helper->baseUrl."/edit-barang.php?id_barang=$stock_barang->id_barang\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$user->id."' >
                                                                        <i class='fa fa-hand-pointer-o'></i>
                                                                    </button></a>
                                                                    <a href=\"".$helper->baseUrl."/list-barang.php?act=delete&id_barang=$stock_barang->id_barang\"<button type='button' class='btn btn-danger ' data-order-id='".$service->id."' >
                                                                        <i class='fa fa-close'></i>
                                                                    </button></a></td>";
    														$html .= "</tr>";
    														
                                                            $total_stock = $total_stock + $stock_barang->jumlah;

    														echo $html;
    														
    														$i++;
    													}
                                                    }
												?>
                                                <tr class="bg-success">
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <td colspan="1"></td>
                                                    <?php endif; ?>
                                                    <td colspan="1" align="center"><b>Total Stock Barang</b></td>
                                                    <td colspan="1" align="right"><b><?= $total_stock; ?></b></td>
                                                    <td colspan="1"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                        <?php
                                            if($p>1)
                                            {
                                                echo "<li><a href=\"".$helper->baseUrl."/listmember.php?p=".($p-1)."\">«</a></li>";
                                            }

                                            for ($i=1; $i <= $totalpage ; $i++) {
                                                if($i == $p){echo "<li><a href=\"?p=".$i."\" class=\"current\">".$i."</a></li>";} 
                                                else {echo "<li><a href=\"?p=".$i."\">".$i."</a></li>";}
                                            }
                                            if($p!=$totalpage)
                                            {
                                                echo "<li><a href=\"".$helper->baseUrl."/listmember.php?p=".($p+1)."\">»</a></li>";
                                            }
                                        ?>
                                         </ul>
                                    </div>
                                    <p><a href="export.php?daftar=<?php echo $pageTitle?>" class="export"><button class="btn btn-info">Export Data to Excel</button></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>