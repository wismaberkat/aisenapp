<?php
    session_start();
	
    if(!isset($_SESSION["username"]) || ($_SESSION["level"] != "Admin"))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Register Karyawan";
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?> </h1>
                </section>
                <section class="content">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<p id="errorAlert">&nbsp;</p>
					</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-7">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Register Karyawan</h3>
                                </div>
								<form id="newMember">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="link">Nama</label>
                                                            <input class="form-control" id="nama" placeholder="Masukkan Nama Lengkap Karyawan" type="text" required />
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="link">Handphone:</label>
                                                            <input class="form-control" id="hp" placeholder="Masukkan Nomor HP Karyawan" type="text" required />
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="username">Username</label>
                                                            <input type="text" class="form-control" id="username" placeholder="Masukkan Username" required />
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="control-label" for="password">Password</label>
                                                            <input type="password" class="form-control" id="password" placeholder="Masukkan Password" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="control-label" for="password">Konfirmasi Passwrod</label>
                                                            <input type="password" class="form-control" id="cPassword" placeholder="Masukkan Password Kembali" required />
                                                        </div>
                                                    </div>
												</div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit" />
                                                    </div>
                                                </div>
                                            </div>
										</div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Result!</h3>
                                </div>
								<div class="box-body" id="registerResult"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>                                            
                    </div>
                </section>
            </div>
			<?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>        
<script type="text/javascript">
	(function($) {
		$.fn.Ticker = function(options)
		{
			var defaults = {
				pause: 5000,
				fadeIn: 800,
				fadeOut: 800,
				delay: 500,
			};
			
			var opts = $.extend({}, defaults, options);
			
			return $(this).each(function() {
				var list = $(this), typewriter = {}, interval;
				
				list.addClass('ticker-active').children(':first').css('display', 'block');
				
				function changeItem() {
					var item = list.children(':first'),
					next = item.next(),
                    copy = item.clone();
					
					clearTimeout(interval);
					
					$(copy).css('display', 'none').appendTo(list);
					
					item.fadeOut(opts.fadeOut, function() {
						$(this).remove();
						
						if (opts.typewriter) {
							typewriter.string = next.text();
							
							next.text('').css('display', 'block');
							
							typewriter.count = 0;
							typewriter.timeout = setInterval(type, opts.speed);
						}
						else {
							next.delay(opts.delay).fadeIn(opts.fadeIn, function () {
								setTimeout(changeItem, opts.pause);
                            });
						}
					});
				}

				function type() {
					typewriter.count++;

					var text =  typewriter.string.substring(0, typewriter.count);

					if (typewriter.count >= typewriter.string.length) {
						clearInterval(typewriter.timeout);
						setTimeout(changeItem, opts.pause);
					}
					else if (opts.cursor) {
						text+= ' ' + opts.cursor;
					}

					list.children(':first').text(text);
				}

				if (list.find('li').length > 1 ) {
					interval = setTimeout(changeItem, opts.pause);
				}
			});
		};

		$('.ticker').Ticker();
	})(jQuery);
	
	$("#newMember").submit(function(e) {
		e.preventDefault();
		
		$("#errorAlert").parent().hide();
		$("#registerResult").empty();

		var nama= $("#nama").val();
		var hp= $("#hp").val();
		var username= $("#username").val();
		var password= $("#password").val();
		var cPassword= $("#cPassword").val();

		$.ajax({
			type: "POST",
			url: "<?= $helper->baseUrl; ?>/api/new-member.php",
			data: 
			{
				nama: nama,
				hp: hp,
				username: username,
				password: password,
				cPassword: cPassword
			},
			success:function(result)
			{
				result = JSON.parse(result);

				if(result.success)
				{
					$("#registerResult").html(result.html);
					
					// $("#sisa_saldo").html(result.saldo);
					// $("#saldo_terpakai").html(result.gunakan);
					// $("#total_order").html(result.total_order);
				}
				else
				{
					$("#errorAlert").parent().show();
					$("#errorAlert").html(result.message);
				}
			}
		});
		
		document.getElementById("newMember").reset();
	});
</script>