<?php  
session_start();
require_once "api/Helpers/GlobalHelper.php";
$helper = new GlobalHelper();

$id = $_GET['id_barang'];

if (isset($_POST['id_barang'])) {
	
	$data = array(
		array("id_barang",$_POST['id_barang']),
		array("id_jenis",$_POST['jenis']),
		array("ukuran",$_POST['ukuran']),
		array("foto", $_POST['foto']),
		array("h_modal",$_POST['modalb']),
		array("h_jual",$_POST['jualb']),
		array("id_supplier",$_POST['supplier']),
		array("keterangan",$_POST['keterangan']),
		array("jumlah",$_POST['stok'])

		);

	$res_update = $helper->database->update("daftar_barang", $data, "id_barang = '$id'");

	if($res_update==1){
		header( "Location:list-barang.php?id=$id&isUpdate=1" );
	} else {
		header( "Location:list-barang.php?id=$id&isUpdate=0" );
	}																		
}
?>