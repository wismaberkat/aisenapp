<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Search";
    $keyword = $_GET["keyword"];
    
?>

<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Hasil Pencarian</h1>
                </section>
                <section class="content">
                    <div class="block">
                        <div class="block">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tabel History Barang Keluar</h3>
                                    <form action="<?php echo "hasil_search_keluar.php?keyword=".$keyword."&tanggal_keluar=".$tanggal_keluar; ?>" method="post">
                                        <div class="col-md-4" style="float:right;">
                                            <div class="input-group">
                                                <input type="text" name="keyword" class="form-control" placeholder="Masukkan Kode Barang/Jenis Barang" required>
                                                <span class="input-group-btn">
                                                <input type="submit" class="btn btn-default" value="Submit">
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                    <?php 
                                    if (!empty($keyword)) {
                                        $res_jenis = $helper->database->select("jenis_barang","id_jenis","jenis_barang='$keyword'");
                                        if (!$res_jenis) {
                                            // echo "bukan mencari jenis";
                                        } else {
                                            foreach($res_jenis as $jenis){
                                                $keywordjenis = $jenis->id_jenis;
                                                // echo "id jenis $keyword adalah $id_jenis";
                                            }
                                            // echo "jenis dicari";
                                        }
                                        // $tanggal_kirim = "10/29/2016";
                                        // $tgl = date("m/d/Y", strtotime($tanggal_kirim));
                                        if (isset($_GET["keyword"])){
                                            if (isset($_GET["tanggal_keluar"])) {
                                                $tanggal_kirim = $_GET["tanggal_keluar"];
                                                $condition = "id_barang='$keyword' OR id_jenis='$keywordjenis' AND tanggal_keluar='$tanggal_kirim'";
                                            } else {
                                                $condition = "id_barang='$keyword' OR id_jenis='$keywordjenis'";
                                            }
                                        }
                                        $res_cari = $helper->database->select("history_keluar","*",$condition,"tanggal_pesan DESC");
                                        if (!$res_cari) {
                                            echo "<p style='color:red;'><b>' $keyword ' tidak ditemukan.</b></p>";
                                        } else {
                                    ?>
                                        <table class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                    <th class="rata-tengah">NO</th>
                                                    <th class="rata-tengah">Tanggal Pemesanan</th>
                                                    <th class="rata-tengah">Tanggal Kirim</th>
                                                    <th class="rata-tengah">Kode Barang</th>
                                                    <th class="rata-tengah">Jenis Barang</th>
                                                    <th class="rata-tengah">Ukuran</th>
                                                    <th class="rata-tengah">Keterangan</th>
                                                    <th class="rata-tengah">Jumlah Keluar</th>
                                                    <th class="rata-tengah">Submitted By</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                        $i = 1;
                                                        foreach ($res_cari as $hasil) {
                                                            $html = "<tr>";
                                                            $html .= "<td>".$i."</td>";
                                                            $html .= "<td align='center'>".$hasil->tanggal_pesan."</td>";
                                                            $html .= "<td align='center'>".$hasil->tanggal_keluar."</td>";
                                                            $html .= "<td align='center'>".$hasil->id_barang."</td>";

                                                            //get jenis_barang
                                                            $id_jenis = $hasil->id_jenis;
                                                            $j_condition = "id_jenis='$id_jenis'";
                                                            $jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);
                                                            foreach($jenis as $jenis_barang)
                                                            {
                                                                $html .= "<td align='center'>".$jenis_barang->jenis_barang."</td>";
                                                            }

                                                            $html .= "<td align='center'>".$hasil->ukuran."</td>";

                                                            $html .= "<td align='left'>".$hasil->keterangan."</td>";
                                                            $html .= "<td align='right'>".$hasil->jumlah."</td>";
                                                            $html .= "<td align='center'>".$hasil->uplink."</td>";
                                                            $html .= "</tr>";
                                                            echo $html;
                                                            $total_stock = $total_stock + $hasil->jumlah;
                                                            $i++;
                                                        }
                                            ?>
                                                <tr class="bg-success">
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1" align="center"><b>Total Stock Barang</b></td>
                                                    <td colspan="1" align="right"><b><?= $total_stock; ?></b></td>
                                                    <td colspan="1"></td>
                                                </tr>
                                            <?php 
                                                }

                                                } else {
                                                    echo "Silahkan Masukkan Kode Barang atau Jenis Barang.";
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>
					<div class="margin-bottom hidden-xs"></div>  	
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
        
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/data-order.js" type="text/javascript"></script>