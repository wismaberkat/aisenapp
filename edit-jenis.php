<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Edit Jenis Barang";
	
    $username = $_SESSION["username"];
    
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Form Edit Jenis Barang</h3>
								</div>
								<?php 
									$id = $_GET['id_jenis'];
								?>
								<form method="post" action="<?php echo "edit-j.php?id_jenis=".$id; ?>">
									<div class="box-body">
									<?php
									$id = $_GET['id_jenis'];
									// echo $id;
									$sql = "SELECT * FROM jenis_barang where id_jenis='$id'";
    								$res = $helper->database->query($sql);
    								if (!$res) {
    									echo "Supplier Tidak";
    								} else {
    									$jenis = $res->fetch_assoc();
    									$ids = $jenis['id_jenis'];
    									// echo $ids;
    								}
								?>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="id_jenis">ID Supplier</label>
													<input type="text" name="id_jenis" class="form-control" id="id_jenis" value="<?=$jenis['id_jenis']?>" readonly>
												</div>
												<div class="form-group">
													<label class="control-label" for="jenis_barang">Nama Jenis Barang</label>
													<input type="text" name="jenis_barang" class="form-control" id="jenis_barang"  value="<?=$jenis['jenis_barang']?>"  />
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right"> Update </button>
												</div> 
											</div>
										</div>
									</div>
								</form>								
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
        <?php include_once "views/templates/scripts.php"; ?>
    </body>
</html>
