<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "History Barang Keluar";
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?> <small>Daftar barang yang keluar.</small></h1>
                </section>
                <section class="content">
                    <div class="block">
                        <div class="block">
                            <div class="box">
                                <!-- <p><a href="export.php"><button>Export Data ke Excel</button></a></p> -->
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <h3 class="box-title" style="margin-bottom:20px;">Tabel History Barang Keluar</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <form action="hasil_search_keluar.php" method="get">
                                            <div class="col-md-4 col-xs-12" style="float:right;">
                                                <div class="input-group">
                                                    <input type="text" name="keyword" class="form-control" placeholder="Masukkan Kode /Jenis Barang" style="float:left;">
                                                    <span class="input-group-btn">
                                                        <input type="submit" class="btn btn-default" value="Cari">
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-4 col-xs-12" style="float:right;">
                                                <div class="form-group">
                                                    <input type="text" id="tanggal_keluar" name="tanggal_keluar" class="form-control" placeholder="Tanggal Kirim" style="float:right;"> -->
                                                    <!-- <span class="input-group-btn">
                                                        <input type="submit" class="btn btn-default btn-lg" value="Cari">
                                                    </span> -->
                                               <!--  </div>
                                            </div> -->
                                        </form>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                    <th class="rata-tengah">No.</th>
                                                    <th class="rata-tengah">Tanggal Pemesanan</th>
                                                    <th class="rata-tengah">Tanggal Kirim</th>
                                                    <th class="rata-tengah">Kode Barang</th>
                                                    <th class="rata-tengah">Jenis Barang</th>
                                                    <th class="rata-tengah">Ukuran</th>
                                                    <th class="rata-tengah">Keterangan</th>
                                                    <th class="rata-tengah">Jumlah keluar</th>
                                                    <th class="rata-tengah">Submitted By</th>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <th class="rata-tengah">Action</th>
                                                    <?php endif; ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
													$i = 1;
													$transaksi = $helper->database->select("history_keluar", "*","1","id_keluar DESC");
													$tgl_psn = "asd";
													foreach($transaksi as $keluar)
                                                    {
                                                        //get jenis_barang
                                                        $idjenis=$keluar->id_jenis;
                                                        $j_condition = "id_jenis='$idjenis'";
                                                        $jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);

                                                        $html = "<tr>";
                                                        $html .= "<td align='right'>".$i."</td>";
                                                        $html .= "<td align='center'>".$keluar->tanggal_pesan."</td>";
                                                        $html .= "<td align='center'>".$keluar->tanggal_keluar."</td>";
														// $html .= "<td align='center'>".$helper->format_date($keluar->tanggal_keluar)."</td>";
                                                        $html .= "<td align='center'>".$keluar->id_barang."</td>";
                                                        foreach ($jenis as $jenis_barang) {
                                                            $html .= "<td align='center'>".$jenis_barang->jenis_barang."</td>";
                                                        }
                                                        $html .= "<td align='center'>".$keluar->ukuran."</td>";
                                                        $html .= "<td align='center'>".$keluar->keterangan."</td>";
                                                        $html .= "<td align='right'>".$keluar->jumlah."</td>";
                                                        $html .= "<td align='center'>".$keluar->uplink."</td>";
                                                        if($_SESSION['level'] == 'Admin'):
                                                        $html .= "<td align='center'><a href=\"".$helper->baseUrl."/edit-keluar.php?id=$keluar->id_keluar\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$user->id."' >
                                                                        <i class='fa fa-hand-pointer-o'></i>
                                                                    </button></a>
                                                                    <a href=\"".$helper->baseUrl."/view-masuk.php?act=delete&id_barang=$stock_barang->id_barang\"<button type='button' class='btn btn-danger ' data-order-id='".$service->id."' >
                                                                        <i class='fa fa-close'></i>
                                                                    </button></a></td>";
                                                        endif;
														$html .= "</tr>";
														
														$total_keluar = $total_keluar + $keluar->jumlah;
														
														echo $html;
														
														$i++;
													}
												?>
												<tr class="bg-success">
													<td colspan="2"></td>
                                                    <td colspan="2"></td>
                                                    <td colspan="1"></td>
                                                    <td colspan="1"></td>
													<td colspan="1" align="center"><b>Total Barang Keluar</b></td>
													<td colspan="1" align="right"><b><?= $total_keluar; ?></b></td>
                                                    <td colspan="1"></td>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <td colspan="1"></td>
                                                    <?php endif; ?>
												</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <p><a href="export.php?historykeluar=<?php echo $pageTitle?>" class="export"><button class="btn btn-info">Export Data to Excel</button></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/jquery-ui.js"></script>
<script>
var dateToday = new Date();
var dates = $("#tanggal_keluar").datepicker({
    defaultDate: "+1d",
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
    }
});
</script>
