<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Home";
?>

<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Welcome, <?= $username; ?></h1>
                </section>
                <section class="content">
                    <?php include_once "views/templates/info_boxes.php"; ?>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
        <?php include_once "views/templates/scripts.php"; ?>
    </body>
</html>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/data-order.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$("#newOrder").submit(function(e) {
		e.preventDefault();
		
		$("#errorAlert").parent().hide();
		$("#orderResult").empty();
		
		var type = $("#orderService option:selected").html();
		var service = $("#orderService").val();
		var jumlah = parseInt($("#jumlah").val());
		var link = $.trim($("#link").val());
		
		if(saldo < harga)
		{
			$("#errorAlert").parent().show();
			$("#errorAlert").html("Failed! Saldo Anda Kurang. Harap melakukan Topup kembali. Silahkan hubungi Admin.");
		}
		else
		{
			$.ajax({
				type: "POST",
				url: "<?= $helper->baseUrl; ?>/api/new-order.php",
				data: 
				{
					typeName: type,
					type: service,
					jumlah: jumlah,
					link: link,
					harga: harga
				},
				success:function(result)
				{
					result = JSON.parse(result);
					
					if(result.success)
					{
						$("#orderResult").html(result.html);
						
						$("#sisa_saldo").html(result.saldo);
						$("#saldo_terpakai").html(result.gunakan);
						$("#total_order").html(result.total_order);
					}
					else
					{
						$("#errorAlert").parent().show();
						$("#errorAlert").html(result.error);
					}
				}
			});
		}
		
		document.getElementById("newOrder").reset();
	});

</script>