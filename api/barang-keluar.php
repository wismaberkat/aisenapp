<?php 
session_start();
if(!isset($_SESSION["username"]))
	header("location:login.php");

require_once "Helpers/GlobalHelper.php";

$helper = new GlobalHelper();

if (isset($_POST["id_barang"])) {

	$id = trim($_POST["id_barang"]);
	$keluar = intval($_POST["jumlah_keluar"]);
	$ket_keluar = trim($_POST["keterangan_keluar"]);
	$uplink = $_SESSION["username"];
	$tanggal_keluar = $_POST["tanggal_kirim"];
	$tanggal_pesan = $helper->date;
	// echo $tanggal_pesan;
	// echo $tanggal_kirim;

	$res_barang = $helper->database->select("daftar_barang","*","id_barang='$id'");

	if (!$res_barang) {
		$notif = "Kode Barang tidak ditemukan";
		header("Location:../barangout.php?id=$id&keluar=0&notif=$notif");
		
	} else {

		foreach ($res_barang  as $barang)
		{	
			$jenis=$barang->id_jenis;
			$ukuran=$barang->ukuran;
			$foto=$barang->foto;
			$modal=$barang->h_modal;
			$jual=$barang->h_jual;
			$supplier=$barang->id_supplier;
			$keterangan=$barang->keterangan;
			$stock_awal = $barang->jumlah;

			if ($keluar <= $stock_awal) {
				$stock_akhir = $stock_awal - $keluar;
				$success="Output Barang Sukses !";
				$update = true;
			} else {
				$error ="Output Barang Gagal ! Jumlah keluar tidak boleh melebihi stok.";
				$stock_akhir = $stock_awal;
				$update = false;
			}

			// echo "Kode Barang = $id "."Jumlah Keluar = $keluar "."Sisa Stok = $stock_akhir "."Pesan = $tanggal_pesan "."Kirim = $tanggal_keluar ";

			$data = array(
				array("jumlah",$stock_akhir)
			);
			$update = $helper->database->update("daftar_barang", $data, "id_barang='$id'");
			$transaksi = array(
				array("id_barang",$id),
				array("ukuran",$ukuran),
				array("id_jenis",$jenis),
				array("keterangan",$ket_keluar),
				array("jumlah",$keluar),
				array("tanggal_pesan",$tanggal_pesan),
				array("tanggal_keluar",$tanggal_keluar),
				array("uplink",$uplink)
			);
			$transaksi = $helper->database->insert("history_keluar",$transaksi);

			if (!$update) {
				echo "error while updating";
				header("Location:../barangout.php?id=$id&keluar=0&notif=$error");
			}
			else {
				echo "updated";
				header("Location:../barangout.php?id=$id&keluar=1&notif=$success");
			}
		}
	}
}
?>