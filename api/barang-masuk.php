<?php
	session_start();
	require_once "Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	$res = new stdClass();
	$res->success = false;
	
	$uplink = $_SESSION["username"];
	$barang = trim($_POST["barang"]);
	$jenis = trim($_POST["jenis"]);
	$ukuran = trim($_POST["ukuran"]);
	$foto = trim($_POST["foto"]);
	$modal = intval(trim($_POST["modal"]));
	$jual = intval(trim($_POST["jual"]));
	$supp = trim($_POST["supp"]);
	$jumlah = intval(trim($_POST["jumlah"]));
	$ket = trim($_POST["ket"]);
	// $created_date = date("Y-m-d H:i:s");
	// $inserted = false;
	// $tercatat = false;
	// $updated = false;

	$res_barang = $helper->database->select("daftar_barang","*","id_barang='$barang'");
	$res_ukuran = $helper->database->select("daftar_barang", "*", "ukuran = '$ukuran'");
	$res_jenis = $helper->database->select("jenis_barang", "*", "id_jenis = '$jenis'");
	$res_supplier = $helper->database->select("supplier", "*", "id_supplier = '$supp'");
	

	if (!$res_barang) {
		//cek jenis dan ukuran yg sama
		// $id_jenis="1";
		// $ukuran = "31";
		// $sql = "SELECT * FROM daftar_barang where id_jenis='$id_jenis' AND ukuran='$ukuran'";
		// $result = $helper->database->query($sql);
		// $jenis = $result->fetch_assoc();
		// $res_ukur = $jenis['ukuran'];
		// $res_jenis = $jenis['id_jenis'];
		// if ($id_jenis!=$res_jenis && $ukuran!=$res_ukur) {
		// 	echo "barang msk";
		// } else {
		// 	echo "brg g msk";
		// }
			//saving
			$data = array(
				array("id_barang", $barang),
				array("id_jenis", $jenis),
				array("ukuran", $ukuran),
				array("foto", $foto),
				array("h_modal", $modal),
				array("h_jual", $jual),
				array("id_supplier", $supp),
				array("jumlah", $jumlah),
				array("keterangan", $ket)
			);
			// $helper->database->insert("stock_barang", $data);
			$input = $helper->database->insert("daftar_barang", $data);

			// $inserted = true;

			$transaksi = array(
				array("id_barang", $barang),
				array("id_supplier", $supp),
				array("ukuran", $ukuran),
				array("id_jenis", $jenis),
				array("tanggal_masuk",$helper->date),
				array("jumlah", $jumlah),
				array("keterangan", $ket),
				array("uplink", $uplink)
			);
			$transaksi = $helper->database->insert("history_masuk", $transaksi);
			// $tercatat = true;
			// if ($inserted == true && $tercatat == true)) {
			// } else {
			// 	$res->success= false;
			// }
			$res->success = true;

			//variabel jenis
			// $cek_jenis = $res_jenis->id_jenis;
			if (!$res_jenis) {
				$nama_jenis="";
			} else {
				foreach ($res_jenis as $jenis ) {
					$nama_jenis = $jenis->jenis_barang;
				}
			}
			//variabel supplier
			if (!$res_supplier) {
				$nama_supplier = "";
			} else {
				foreach ($res_supplier as $supplier ) {
					$nama_supplier = $supplier->nama_supplier;
				}
			}

			$res->html = "<table class='table table-bordered alert alert-success' style='display: block !important;'>
							<thead>
								<tr style='border-top:1px solid white;'>
									<th colspan='2'>
										<i class='fa fa-check-circle'></i> Sukses menambahkan barang baru.<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='color:white;'>&times;</button>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><td class='col-sm-4'>Kode Barang</td> <td><b>$barang</b></td></tr>
								<tr><td>Jenis Barang</td> <td><b>$nama_jenis</b></td></tr>
								<tr><td>Ukuran</td> <td><b>$ukuran</b></td></tr>
								<tr><td>Supplier</td> <td><b>$nama_supplier</b></td></tr>
								<tr><td>Jumlah Masuk</td> <td><b>$jumlah</b></td></tr>
								<tr><td>Keterangan</td> <td><b>$ket</b></td></tr>
							</tbody>
						</table>";
	} else {
		//update
		// $updatenotif = "<table class='table table-bordered alert alert-success' style='display: block !important;'>
		// 					<thead>
		// 						<tr style='border-top:1px solid white;'>
		// 							<th colspan='2'>
		// 								<i class='fa fa-check-circle'></i> Sukses Menambahkan Stok Barang.<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='color:white;'>&times;</button>
		// 							</th>
		// 						</tr>
		// 					</thead>
		// 					<tbody>
		// 						<tr><td class='col-sm-4'>Kode Barang</td> <td><b>$barang</b></td></tr>
		// 						<tr><td>Jenis Barang</td> <td><b>$nama_jenis</b></td></tr>
		// 						<tr><td>Ukuran</td> <td><b>$ukuran</b></td></tr>
		// 						<tr><td>Supplier</td> <td><b>$nama_supplier</b></td></tr>
		// 						<tr><td>Jumlah Awal</td> <td><b>$stock_awal</b></td></tr>
		// 						<tr><td>Jumlah Sekarang</td> <td><b>$stock_akhir</b></td></tr>
		// 						<tr><td>Keterangan</td> <td><b>$ket</b></td></tr>
		// 					</tbody>
		// 				</table>";
		// $headnotif = "<table class='table table-bordered alert alert-danger' style='display: block !important;'>
		// 			<thead>
		// 				<tr style='border-top:1px solid white;'>
		// 					<th colspan='2'>
		// 						<i class='fa fa-close'></i> Gagal Input Barang ID!<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='color:white;'>&times;</button>
		// 					</th>
		// 				</tr>
		// 			</thead>
		// 			";
		// $headnotif_id = "<table class='table table-bordered alert alert-danger' style='display: block !important;'>
		// 			<thead>
		// 			<tr style='border-top:1px solid white;'>
		// 			<th colspan='2'>
		// 			<i class='fa fa-close'></i> Gagal Input Barang Jenis!<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='color:white;'>&times;</button>
		// 			</th>
		// 			</tr>
		// 			</thead>
		// 			";
		// $bodynotif = "<tbody>
		// 			<tr><td class='col-sm-4'>ID Barang</td> <td><b>asd</b></td></tr>
		// 			<tr><td>Jenis Barang</td> <td><b>asd</b></td></tr>
		// 			<tr><td>Ukuran</td> <td><b>asd</b></td></tr>
		// 			<tr><td>Supplier</td> <td><b>asd</b></td></tr>
		// 			<tr><td>Jumlah Masuk</td> <td><b>asd</b></td></tr>
		// 			<tr><td>Keterangan</td> <td><b>asd</b></td></tr>
		// 			</tbody>
		// 			</table>";

		// foreach($res_barang as $baranglama){
		// 	$cek_id = $baranglama->id_barang;
		// 	$cek_jenis = $baranglama->id_jenis;
		// 	$cek_ukur = $baranglama->id_ukur;
		// 	$stock_awal = $baranglama->jumlah;

		// 	if ($cek_id==$barang) {
		// 		if ($cek_jenis==$jenis) {
		// 			if ($cek_ukur==$ukuran) {
		// 				if ($cek_supp = $supp) {
		// 					$stock_akhir = $stock_awal + $jumlah;
		// 					$data = array(
		// 						array("jumlah", $stock_akhir)
		// 						);
		// 					$input = $helper->database->update("daftar_barang", $data);
		// 					$updated = true;
		// 					$data = array(
		// 						array("id_barang", $barang),
		// 						array("id_supplier", $supp),
		// 						array("ukuran", $ukuran),
		// 						array("id_jenis", $jenis),
		// 						array("tanggal_masuk",$helper->date),
		// 						array("jumlah", $jumlah),
		// 						array("keterangan", $ket),
		// 						array("uplink", $uplink)
		// 						);
		// 					$history = $helper->database->insert("history_masuk", $history);
		// 					$res->html = $updatenotif;
		// 				} else {
		// 					//supp tidak sama
		// 					$updated = false;
		// 				}
		// 			} else {
		// 				//ukuran tidak sama 
		// 				$updated = false;
		// 				$res->html = $notif;
		// 			}
		// 		} else {
		// 			//jenis tidak sama
		// 			$updated = false;
		// 			$res->html = $notif;
		// 		}
		// 	} else {
		// 		//id tidak sama
		// 		$updated = false;
		// 		$res->html = $notif;
		// 	}
			
		// }
	}
	echo json_encode($res);
?>