<table border="1">
	<thead>
		<tr>
			<?php 
				if ($_SESSION['level'] == 'Admin') {
					echo '<td colspan="9" align="center" style="font-size:20px;"><b>TABEL DAFTAR BARANG</b></td>';
				} else {
					echo '<td colspan="8" align="center" style="font-size:20px;"><b>TABEL DAFTAR BARANG</b></td>';
				}
			?>
		</tr>
	</thead>
	<thead>
		<tr>
			<th>No.</th>
			<th>KODE BARANG</th>
			<th>JENIS BARANG</th>
			<th>UKURAN</th>
			<?php if($_SESSION['level'] == 'Admin'): ?>
			<th>BP</th>
			<?php endif; ?>
			<th>HARGA JUAL</th>
			<th>SUPPLIER</th>
			<th>KETERANGAN</th>
			<th>STOK</th>
		</tr>
	</thead>
	<tbody>
		
	<?php
	session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");

	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$i = 1;
	$barang = $helper->database->select("daftar_barang", "*","1");
	if (!$barang) {
		echo "tidak ada stok.";
	} else {
		foreach($barang as $stock_barang)
		{
            //get jenis_barang
			$id_jenis = $stock_barang->id_jenis;
			$j_condition = "id_jenis='$id_jenis'";
			$jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);

			echo '<tr>';
			echo '<td>'.$i.'</td>';
			echo '<td align="center">'.$stock_barang->id_barang.'</td>';

			foreach($jenis as $jenis_barang)
			{
				echo '<td align="center">'.$jenis_barang->jenis_barang.'</td>';
			}

			echo '<td align="center">'.$stock_barang->ukuran.'</td>';

			if($_SESSION['level'] == 'Admin'):
				echo '<td align="right">'.$helper->format_total_rupiah($stock_barang->h_modal).'</td>';
			endif;
			echo '<td align="right">'.$helper->format_total_rupiah($stock_barang->h_jual).'</td>';

            //get nama_supplier
			$id_supplier = $stock_barang->id_supplier;
			$s_condition = "id_supplier='$id_supplier'";
			$supp = $helper->database->select("supplier", "nama_supplier",$s_condition);
			foreach($supp as $supplier)
			{
				echo '<td align="center">'.$supplier->nama_supplier.'</td>';
			}

			echo '<td align="center">'.$stock_barang->keterangan.'</td>';
			echo '<td align="right">'.$stock_barang->jumlah.'</td>';
			echo '</tr>';

			$total_stock = $total_stock + $stock_barang->jumlah;

			$i++;
		}
	}
	?>
		<tr class="bg-success">
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<?php if($_SESSION['level'] == 'Admin'): ?>
			<td colspan="1"></td>
			<?php endif; ?>
			<td colspan="1" align="center"><b>Total Stock Barang</b></td>
			<td colspan="1" align="right"><b><?= $total_stock; ?></b></td>
		</tr>
	</tbody>
</table>