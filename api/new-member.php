<?php
	session_start();
	
	require_once "Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$return = new stdClass();
	$return->success = false;
	
	$nama = trim($_POST["nama"]);
	$email = trim($_POST["email"]);
	$alamat = trim($_POST["alamat"]);
	$hp = trim($_POST["hp"]);
	$username = trim($_POST["username"]);
	$password = trim($_POST["password"]);
	$cPassword = trim($_POST["cPassword"]);
	$uplink = $_SESSION["username"];
	// $profile = $helper->getProfileData($uplink);
	
 
	if($password != $cPassword)
	{
		$return->message = "Failed! Password dan Konfirmasi Password tidak sama.";
	}
	else
	{
		$registeredUsername = $helper->database->select("user", "COUNT(id) AS matched", "username = '$username'");
		$registeredEmail = $helper->database->select("user", "COUNT(id) AS matched", "email = '$email'");
		
		if($registeredUsername[0]->matched > 0)
		{
			$return->message = "Pendaftaran karyawan gagal, username telah terdaftar";
		}
		else if($registeredEmail[0]->matched > 0)
		{
			$return->message = "Pendaftaran karyawan gagal, email telah terdaftar";
		}
		else
		{
			$data = array(
				array("nama", $nama),
				array("hp", $hp),
				array("username", $username),
				array("password", $password),
				array("level", "Karyawan"),
				array("uplink", $uplink)
			);

			$return->id = $helper->database->insert("user", $data);

			$return->success = true;

			$return->html = "<table class='table table-bordered alert alert-success' style='display: block !important;'>
								<thead>
									<tr style='border-top:1px solid white;'>
										<th colspan='2'>
											<i class='fa fa-check-circle'></i> Tambah Karyawan Sukses !
											<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr><td class='col-sm-4'>Nama</td> <td><b>$nama</b></td></tr>
									<tr><td>Username/Link</td> <td><b>$username</b></td></tr>
									<tr><td>Password</td> <td><b>$password</b></td></tr>
									<tr><td>Uplink</td> <td><b>$uplink</b></td></tr>
								</tbody>
							</table>";
		}
	}
	echo json_encode($return);
?>