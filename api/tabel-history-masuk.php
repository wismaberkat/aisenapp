<table border="1">
	<thead>
		<tr>
			<?php 
			session_start();

			if(!isset($_SESSION["username"])){
				header("location:login.php");
			}
			if ($_SESSION['level'] == 'Admin') {
				echo '<td colspan="9" align="center" style="font-size:20px;"><b>TABEL DAFTAR BARANG</b></td>';
			} else {
				echo '<td colspan="8" align="center" style="font-size:20px;"><b>TABEL DAFTAR BARANG</b></td>';
			}
			?>
		</tr>
	</thead>
	<thead>
		<tr>
			<th>No.</th>
			<th>TANGGAL MASUK</th>
			<th>KODE BARANG</th>
			<th>JENIS BARANG</th>
			<th>UKURAN</th>
			<?php if($_SESSION['level'] == 'Admin'): ?>
			<th>SUPPLIER</th>
			<?php endif; ?>
			<th>KETERANGAN</th>
			<th>SUBMITTED BY</th>
			<th>JUMLAH MASUK</th>
		</tr>
	</thead>
	<tbody>
		
	<?php


	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$i = 1;
	$barang = $helper->database->select("history_masuk", "*");
	if (!$barang) {
		echo "tidak ada stok.";
	} else {
		foreach($barang as $history)
		{
            //get jenis_barang
			$id_jenis = $history->id_jenis;
			$j_condition = "id_jenis='$id_jenis'";
			$jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);

			echo '<tr>';
			echo '<td>'.$i.'</td>';
			echo '<td align="center">'.$history->tanggal_masuk.'</td>';
			echo '<td align="center">'.$history->id_barang.'</td>';

			foreach($jenis as $jenis_barang)
			{
				echo '<td align="center">'.$jenis_barang->jenis_barang.'</td>';
			}

			echo '<td align="center">'.$history->ukuran.'</td>';

            //get nama_supplier
			$id_supplier = $history->id_supplier;
			$s_condition = "id_supplier='$id_supplier'";
			$supp = $helper->database->select("supplier", "nama_supplier",$s_condition);
			foreach($supp as $supplier)
			{
				if($_SESSION['level'] == 'Admin'):
				echo '<td align="center">'.$supplier->nama_supplier.'</td>';
				endif;
			}

			echo '<td align="center">'.$history->keterangan.'</td>';
			echo '<td align="center">'.$history->uplink.'</td>';
			echo '<td align="right">'.$history->jumlah.'</td>';
			echo '</tr>';

			$total_stock = $total_stock + $history->jumlah;

			$i++;
		}
	}
	?>
		<tr>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<?php if($_SESSION['level'] == 'Admin'): ?>
			<td colspan="1"></td>
			<?php endif; ?>
			<td colspan="1" align="center"><b>Total Stock Barang</b></td>
			<td colspan="1" align="right"><b><?= $total_stock; ?></b></td>
		</tr>
	</tbody>
</table>