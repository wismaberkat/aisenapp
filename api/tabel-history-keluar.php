<table border="1">
	<thead>
		<tr>
			<td colspan="9" align="center" style="font-size:20px;"><b>TABEL DAFTAR BARANG</b></td>
		</tr>
	</thead>
	<thead>
		<tr>
			<th>NO</th>
			<th>TANGGAL PEMESANAN</th>
			<th>TANGGAL KIRIM</th>
			<th>KODE BARANG</th>
			<th>JENIS BARANG</th>
			<th>UKURAN</th>
			<th>KETERANGAN</th>
			<th>SUBMITTED BY</th>
			<th>JUMLAH KELUAR</th>
		</tr>
	</thead>
	<tbody>
		
	<?php


	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$i = 1;
	$barang = $helper->database->select("history_keluar", "*");
	if (!$barang) {
		echo "tidak ada stok.";
	} else {
		foreach($barang as $history)
		{
			echo '<tr>';
			echo '<td>'.$i.'</td>';
			echo '<td align="center">'.$history->tanggal_pesan.'</td>';
			echo '<td align="center">'.$history->tanggal_keluar.'</td>';
			echo '<td align="center">'.$history->id_barang.'</td>';

            //get jenis_barang
			$id_jenis = $history->id_jenis;
			$j_condition = "id_jenis='$id_jenis'";
			$jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);
			foreach($jenis as $jenis_barang)
			{
				echo '<td align="center">'.$jenis_barang->jenis_barang.'</td>';
			}

			echo '<td align="center">'.$history->ukuran.'</td>';
			echo '<td align="center">'.$history->keterangan.'</td>';
			echo '<td align="right">'.$history->uplink.'</td>';
			echo '<td align="right">'.$history->jumlah.'</td>';
			echo '</tr>';

			$total_stock = $total_stock + $history->jumlah;

			$i++;
		}
	}
	?>
		<tr class="bg-success">
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1"></td>
			<td colspan="1" align="center"><b>Total Barang Keluar</b></td>
			<td colspan="1" align="right"><b><?= $total_stock; ?></b></td>
		</tr>
	</tbody>
</table>