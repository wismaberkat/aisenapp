<?php 
    require_once "/Helpers/GlobalHelper.php";
    
    $helper = new GlobalHelper();
?>
<!DOCTYPE html>
<html>
<head>
    
<style>
table {
    width: 100%;
    border-collapse: collapse;
    /*background-color: white;*/
}

table, td, th {
    border: 1px solid black;
    padding: 5px;
}

th {text-align: left;}
</style>
</head>
<body>

<?php
    $jenis = $_GET['jenis'];
    $cari_barang = $helper->database->select("daftar_barang","*","id_jenis='$jenis'");
?>
<div class="box">
<div class="box-body">
    <div class="table-resposive">
        <?php 
            if (!$cari_barang) {
                echo "<p style='color:red;'><b>Jenis Barang Tidak Tersedia.</b></p>";
            } else {


        ?>
        <table class="table table-bordered table-hover table-striped table-font-size">
            <thead>
                <tr>
                    <th class="rata-tengah">NO</th>
                    <th class="rata-tengah">ID</th>
                    <th class="rata-tengah">Jenis Barang</th>
                    <th class="rata-tengah">Ukuran</th>
                    <th class="rata-tengah">Foto</th>
                    <?php if($_SESSION['level'] == 'Admin'): ?>
                    <th class="rata-tengah">BP</th>
                    <?php endif; ?>
                    <th class="rata-tengah">Harga Jual</th>
                    <th class="rata-tengah">Supplier</th>
                    <th class="rata-tengah">Keterangan</th>
                    <th class="rata-tengah">Stok</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i = 1;
                foreach($cari_barang as $cari)
                {
                    $html = "<tr>";
                    $html .= "<td>".$i."</td>";
                    $html .= "<td>".$cari->id_barang."</td>";
                    //get nama jenis
                    $id_jenis = $cari->id_jenis;
                    $j_condition = "id_jenis='$id_jenis'";
                    $jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);
                    foreach($jenis as $jenis_barang)
                    {
                        $html .= "<td align='center'>".$jenis_barang->jenis_barang."</td>";
                    }
                    // $html .= "<td align='center'>".$cari->id_jenis."</td>";
                    $html .= "<td align='center'>".$cari->ukuran."</td>";
                    $html .= "<td align='center'>".$cari->foto."</td>";
                    if($_SESSION['level'] == 'Admin'):
                        $html .= "<td align='right'>".$helper->format_rupiah($cari->h_modal)."</td>";
                    endif;
                    $html .= "<td align='right'>".$helper->format_rupiah($cari->h_jual)."</td>";
                    //get nama supplier
                    $id_supplier = $cari->id_supplier;
                    $s_condition = "id_supplier='$id_supplier'";
                    $supp = $helper->database->select("supplier", "nama_supplier",$s_condition);
                    foreach($supp as $supplier)
                    {
                        $html .= "<td align='center'>".$supplier->nama_supplier."</td>";
                    }
                    $html .= "<td align='left'>".$cari->keterangan."</td>";
                    $html .= "<td align='right'>".$cari->jumlah."</td>";
                    $html .= "</tr>";
                    $total_stock = $total_stock + $cari->jumlah;
                    echo $html;
                    $i++;
                }
              
                ?>
                <tr class="bg-success">
                    <td colspan="1"></td>
                    <td colspan="1"></td>
                    <td colspan="1"></td>
                    <td colspan="1"></td>
                    <td colspan="1"></td>
                    <td colspan="1"></td>
                    <td colspan="1"></td>
                    <?php if($_SESSION['level'] == 'Admin'): ?>
                    <td colspan="1"></td>
                    <?php endif; ?>
                    <td colspan="1" align="center"><b>Total Stock Barang</b></td>
                    <td colspan="1" align="right"><b><?= $total_stock; ?></b></td>
                </tr>
            </tbody>
        </table>
        <?php 
            }
        ?>
    </div>
</div>
</div>
</body>
</html>