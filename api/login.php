<?php
	session_start();
	
	require_once "Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$return = new stdClass();
	$return->success = true;
	
	$userid = $helper->database->escapestr($_POST["userid"]);
	$password = $helper->database->escapestr($_POST["password"]);
	

	$result = $helper->database->select("user", "*, COUNT(id) as matched", "(username = '$userid') && password = '$password'");
	
	if(!isset($result[0]->error)) 
	{
		if($result[0]->matched > 0)
		{
			$username = $result[0]->username;
			
			$_SESSION["nama"] = $result[0]->nama;
			$_SESSION["email"] = $result[0]->email;
			$_SESSION["hp"] = $result[0]->hp;
			$_SESSION["username"] = $result[0]->username;
			$_SESSION["level"] = $result[0]->level;
			// $_SESSION["key"] = $result[0]->key;
			
			$return->nama = $result[0]->nama;
		}
		else
		{
			$return->success = false;
			$return->message = "Username atau Password salah";
		}
	}
	else
	{
		$return->success = false;
		$return->message = $result[0]->message;
	}
	
	echo json_encode($return);
?>