<?php 
session_start();
if(!isset($_SESSION["username"]))
	header("location:login.php");

require_once "Helpers/GlobalHelper.php";

$helper = new GlobalHelper();
$inserted = false;
$tercatat = false;

if (isset($_POST["barang"]) && isset($_POST["jenis"]) && isset($_POST["ukuran"]) ) {

	$uplink = $_SESSION["username"];
	$barang = trim($_POST["barang"]);
	$jenis = trim($_POST["jenis"]);
	$ukuran = trim($_POST["ukuran"]);
	$foto = $_POST["photo"];
	$modal = intval(trim($_POST["modal"]));
	$jual = intval(trim($_POST["jual"]));
	$supp = trim($_POST["supp"]);
	$jumlah = intval(trim($_POST["jumlah"]));
	$ket = trim($_POST["ket"]);


	
	
	$res_barang = $helper->database->select("daftar_barang","*","id_barang='$barang'");
	$res_ukuran = $helper->database->select("daftar_barang", "*", "ukuran = '$ukuran'");
	$res_jenis = $helper->database->select("jenis_barang", "*", "id_jenis = '$jenis'");
	$res_supplier = $helper->database->select("supplier", "*", "id_supplier = '$supp'");

	if (!$res_barang) {

		$valid_file  = true;
		$max_size  = 10240000;
		//get photo
		if($_FILES['photo']['name']){
			if(!$_FILES['photo']['error']){
				$new_file_name = strtolower($_FILES['photo']['tmp_name']);
	      		//validasi ukuran foto
				if($_FILES['photo']['size'] > $max_size){
					$valid_file = false;
					$message = 'Maaf, file terlalu besar. Max: 10MB';
				}
				$image_path = pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
				$extension = strtolower($image_path);
	      		//validasi tipe gambar
				if($extension != "jpg" && $extension != "jpeg" && $extension != "png" && $extension != "gif" ){
					$valid_file = false;
					$message = "Maaf, file yang diijinkan hanya format JPG, JPEG, PNG & GIF. #".$extension;
				}

				if($valid_file == true){
	        	//Upload  jadi BLOB
					// $db = mysqli_connect("localhost","root","mysql","test_upload");
					$image = addslashes(file_get_contents($_FILES['photo']['tmp_name']));
					$query = "INSERT INTO daftar_barang (foto) VALUES ('$image')";
					// $qry = mysqli_query($db,$query);
					// if (!$qry) {
					// 	$notif="error saving image";
	    //       			echo "error saving image";
					// 	header( "Location:proses_view.php?notif=$notif" );
					// } else {
					// 	$notif="saving success !";
	    //       			echo "saving success!";
					// 	header( "Location:proses_view.php?notif=$notif&view=1" );
					// }
				}
			}else{
				$message = 'Ooops!  Upload Error:  '.$_FILES['photo']['error'];
			}
		}
		echo "Username = $uplink "."Kode barang = $barang "."Jenis Barang = $jenis "."Ukuran = $ukuran "."Foto = $image "."BP = $modal "."Jual = $jual ".
		"Supplier = $supp "."Jumlah = $jumlah "."Keterangan = $ket";
		// echo $image;
		//saving
		$data = array(
			array("id_barang", $barang),
			array("id_jenis", $jenis),
			array("ukuran", $ukuran),
			array("foto", $image),
			array("h_modal", $modal),
			array("h_jual", $jual),
			array("id_supplier", $supp),
			array("keterangan", $ket),
			array("jumlah", $jumlah)
		);
		$input = $helper->database->insert("daftar_barang", $data);
		// $input_foto = $helper->database->query($query);
		// if (!$input_foto) {
		// 	echo "gagal save foto";
		// } else {
		// 	echo "sukses save foto";
		// }
		// if (!$input) {
		// 	echo "gagal input ke daftar_barang";
		// }
		// $inserted = true;
		//insert ke stock_masuk
		// $transaksi = array(
		// 		array("id_barang", $barang),
		// 		array("id_supplier", $supp),
		// 		array("ukuran", $ukuran),
		// 		array("id_jenis", $jenis),
		// 		array("jumlah", $jumlah)
		// 	);
		$transaksi = array(
			array("id_barang", $barang),
			array("id_supplier", $supp),
			array("ukuran", $ukuran),
			array("id_jenis", $jenis),
			array("tanggal_masuk",$helper->date),
			array("jumlah", $jumlah),
			array("keterangan", $ket),
			array("uplink", $uplink)
		);
		$transaksi = $helper->database->insert("history_masuk", $transaksi);
		// if (!$transaksi) {
		// 	echo "gagal input ke history masuk";
		// }
		// $tercatat = true;

		// if ($inserted) {
		// 	if ($tercatat) {
		// 		header("Location:../barangin.php?masuk=1&transaksi=1");
		// 	} else {
		// 		header("Location:../barangin.php?masuk=1&transaksi=0");
		// 	}
		// } else {
		// 	if (!$tercatat) {
		// 		header("Location:../barangin.php?masuk=0&transaksi=0");
		// 	} else {
		// 		header("Location:../barangin.php?masuk=0&transaksi=1");
		// 	}
		// }

		// if (!$input) {
		// 	echo "a";
		// } else {
		// 	echo "b";
		// }
		// if (!$transaksi) {
		// 	echo "1";
		// } else {
		// 	echo "2";
		// }

		// if (!$insert) {
		// 	if (!$transaksi) {
		// 		header("Location:../barangin.php?masuk=0&transaksi=0");
		// 	} else {
		// 		header("Location:../barangin.php?masuk=0&transaksi=1");
		// 	}
		// } else {
		// 	if (!$transaksi) {
		// 		header("Location:../barangin.php?masuk=1&transaksi=0");
		// 	} else {
		// 		header("Location:../barangin.php?masuk=1&transaksi=1");
		// 	}
		// }

	} else {
		//update di daftar_barang
	}
	
}
?>