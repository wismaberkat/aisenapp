<?php
	class DatabaseHelper extends MySQLi
	{
		private $host, $user, $password, $database, $port, $socket, $result;
		
		public function __construct($dbHost, $dbUser, $dbPass, $dbName, $dbPort, $dbSocket)
		{
			$this->host = $dbHost;
			$this->user = $dbUser;
			$this->password = $dbPass;
			$this->database = $dbName;
			$this->port = NULL;
			$this->socket = NULL;
			
			$this->result[0] = new stdClass();
			$this->result[0]->error = true;
			
			$this->connect($this->host, $this->user, $this->password, $this->database, $this->port, $this->socket);
		}
		
		private function extracts($data)
		{
			$column = array("","");
			
			foreach($data as $index => $details)
			{
				$column[0].= $details[0].",";
				$column[1].= "'".$this ->real_escape_string($details[1])."',";
			}
			
			$column[0] = rtrim($column[0],",");
			$column[1] = rtrim($column[1],",");
			
			return $column;
		}
		
		private function append($data)
		{
			$string = "";
			
			foreach($data as $index => $details)
			{
				$string.= $details[0]."='".$this->real_escape_string($details[1])."',";
			}
			
			$string = rtrim($string,",");
			
			return $string;
		}
		
		public function insert($table, $data)
		{
			$extracted = $this->extracts($data);
			
			$this->query("INSERT INTO ".$table."(".$extracted[0].") VALUES(".$extracted[1].")");
			
			if($this->error)
			{
				$this->result[0]->message = $this->error;
				
				return $this->result;
			}
			else
				return $this->insert_id;
		}
		
		public function update($table, $data, $where)
		{
			$extracted = $this->append($data);
			
			$this->query("UPDATE ".$table." SET ".$extracted." WHERE ".$where);
			
			if($this->error)
			{
				$this->result[0]->message = $this->error;	
				
				return $this->result;
			}
			else
				return true;
		}
		
		public function select($table, $column = "", $where = 1, $orderby = "", $limit = "")
		{
			$column = ($column == "") ? "*" : $column;
			$orderby = ($orderby == "") ? "" : "ORDER BY ".$orderby;
			$limit = ($limit == "") ? "" : "LIMIT ".$limit;
			
			$result = $this->query("SELECT ".$column." FROM ".$table." WHERE ".$where." ".$orderby." ".$limit);
			
			if($this->error)
			{
				$this->result[0]->message = $this->error;	
				return $this->result;
			}
			else
			{
				$data = [];
				$index = 0;
				
				while(($obj = mysqli_fetch_object($result)) != NULL ) {
					$data[$index] = $obj;
					$index++;
				}
				
				return $data;
			}
		}
		
		public function delete($table, $where)
		{
			$this->query("DELETE FROM ".$table." WHERE ".$where."");
			
			if($this->error)
			{
				$this->result[0]->message = $this->error;	
				return $this->result;
			}
			else
				return true;
		}
		
		public function escapestr($string)
{
	return $this->real_escape_string($string);
}
	}
?>