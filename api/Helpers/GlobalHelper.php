<?php
	require_once "DatabaseHelper.php";
	
	class GlobalHelper
	{
		public $baseUrl;
		public $title;
		// public $key;
		
		public $mailHost;
		public $mailPort;
		public $mailUser;
		public $mailPassword;
		
		public $database;
		
		public $date;
		
		public function __construct()
		{
			date_default_timezone_set("Asia/Jakarta");
			$this->baseUrl = "http://168.10.10.244/aisen";
			// $this->baseUrl = "http://192.168.0.106/aisen";
			$this->title = "AISEN";
			
			$this->dbHost = "localhost";
			$this->dbUser = "root";
			$this->dbPass = "mysql";
			$this->dbName = "aisen";
			$this->dbPort = NULL;
			$this->dbSocket = NULL;
			
			$this->date = date("m/d/Y");
			
			$this->database = new DatabaseHelper($this->dbHost, $this->dbUser, $this->dbPass, $this->dbName, $this->dbPort, $this->dbSocket);
		}
		
		public function format_total_rupiah($value)
		{
			return "Rp " . number_format($value, 0, '.', '.'); 
		}
		public function format_rupiah($value)
		{
			return "" . number_format($value, 0, '.', '.'); 
		}
		public function format_date($date)
		{
			return date("m/d/Y", strtotime($date));
		}
		
		public function isActive($menu)
		{
			$link = $_SERVER["PHP_SELF"];
			$link_array = explode('/',$link);
			$page = end($link_array);
			
			return ($page == $menu) ? 'class="active"' : '';
		}
		
		public function getProfileData($username)
		{
			return $this->database->select("user", "*", "username = '$username'")[0];
		}
		
		public function getTotalOrder($username)
		{
			return $this->database->select("stock_barang", "COUNT(id) AS total_order", "username = '$username'")[0]->total_order;
		}
	}
?>