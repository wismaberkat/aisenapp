<?php  
session_start();
require_once "api/Helpers/GlobalHelper.php";
$helper = new GlobalHelper();

$id = $_GET['id_masuk'];

if (isset($_POST['id_masuk'])) {
	
	$id_masuk_awal = $_POST['id_masuk_awal'];
	$id_masuk = $_POST['id_masuk'];
	$id_barang_baru = $_POST['id_barang'];
	$id_barang_lama = $_POST['id_barang_lama'];
	$id_jenis = $_POST['id_jenis'];
	$ukuran = $_POST['ukuran'];
	$id_supplier = $_POST['id_supplier'];
	$jumlah_masuk = $_POST['jumlah'];
	$keterangan = $_POST['keterangan'];
	$uplink	= $_SESSION["username"];
	// echo "$id_masuk "."$id_barang "."$id_jenis "."$ukuran "."$id_supplier "."$tanggal_masuk "."$jumlah_masuk "."$keterangan "."$uplink "."$id_masuk_awal ";
	echo "$id_barang_baru"."$id_barang_lama<br>";

	//cek id_barang_baru di daftar_barang
	$res_barang_baru = $helper->database->select("daftar_barang","*","id_barang ='$id_barang_baru'");
	if (!$res_barang_baru) {
		//stop karna id_barang_baru hasil edit tidak ada di tabel daftar_barang
		$notif="kode barang yang dimasukkan tidak terdaftar. tidak dapat menambah/mengurangi stok pada daftar barang<br>";
	} else {
		//lanjut karna id_barang_baru hasil edit ada di tabel daftar_barang dan stok bisa di update
		// echo "kode barang yang dimasukkan terdaftar<br>";

		//get jumlah di id_barang_lama untuk dihitung
		$res_barang_lama = $helper->database->select("daftar_barang","*","id_barang='$id_barang_lama'");

		//get jumlah barang masuk
		$res_history = $helper->database->select("history_masuk","*","id_masuk='$id_masuk_awal'");

		foreach ($res_barang_baru as $barang_baru){
			//get jumlah di id_barang baru untuk dihitung
			$jumlah_awal_barang_baru = $barang_baru->jumlah;
		}
		if ($res_history) {
			foreach($res_history as $history){

				$masuk_awal = $history->jumlah;
			}
		} else {
			echo "data history masuk error<br>";
		}
		if ($res_barang_lama) {
			foreach($res_barang_lama as $barang_lama){

				$jumlah_awal_barang_lama = $barang_lama->jumlah;
			}
		} else {
			echo "data daftar barang error<br>";
		}

		//pengurangan jumlah stok untuk stok id_barang_lama
		$jumlah_akhir_barang_lama = $jumlah_awal_barang_lama - $masuk_awal;

		//penambahan jumlah stok untuk stok_id_barang_baru
		$jumlah_akhir_barang_baru = $jumlah_awal_barang_baru + $jumlah_masuk;
		// echo $jumlah_akhir;

		echo "(id_barang_lama) $id_barang_lama "."awal = $jumlah_awal_barang_lama "."jumlah masuk = $masuk_awal "."setelah dikurang = $jumlah_akhir_barang_lama";
		echo "<br>";
		echo "(id_barang_baru) $id_barang_baru ". "awal = $jumlah_awal_barang_baru "."jumlah masuk = $masuk_awal "."setelah ditambah = $jumlah_akhir_barang_baru";
		echo "<br>";
	
		$update_history = array(
			array("id_barang",$id_barang_baru),
			array("id_jenis",$id_jenis),
			array("ukuran",$ukuran),
			array("id_supplier",$id_supplier),
			array("jumlah",$jumlah_masuk),
			array("keterangan",$keterangan),
			array("uplink",$uplink)
		);
		// $res_update = $helper->database->update("history_masuk", $update_history, "id_masuk='$id_masuk_awal'");
		$res_update =1;
		$update_stok_id_baru = array(
			array("id_barang",$id_barang_baru),
			array("jumlah",$jumlah_akhir_barang_baru)
		);
		// $update_id_baru = $helper->database->update("daftar_barang",$update_stok_id_baru,"id_barang='$id_barang_baru'");
		$update_id_baru = 1;
		$update_stok_id_lama = array(
			array("jumlah",$jumlah_akhir_barang_lama)
		);
		// $update_id_lama = $helper->database->update("daftar_barang",$update_stok_id_lama,"id_barang='$id_barang_lama'");
		$update_id_lama=1;
		if ($res_update==1 && $update_id_baru==1 && $update_id_lama==1) {
			$notif = "History dengan Kode Barang $id_barang_baru berhasil di edit";
			header( "Location:viewmasuk.php?updated=$id&updated=1&notif=$notif" );
		} else {
			$notif = "History dengan Kode Barang $id_barang_baru gagal di edit";
			header( "Location:viewmasuk.php?updated=$id&updated=0&notif=$notif" );

		}
		// if ($res_update==1 && $update_id_baru==1 && $update_id_lama==1) {
			
		// } else if ($res_update==1 && $update_id_baru==1 && $update_id_lama==0){

		// } else if ($res_update==1 && $update_id_baru==0 && $update_id_lama==0) {
			
		// } else if ($res_update==0 && $update_id_baru==0 && $update_id_lama==0) {
			
		// } else if ($res_update==0 && $update_id_baru==1 && $update_id_lama==1) {
			
		// } else if ($res_update==0 && $update_id_baru==0 && $update_id_lama==1) {
			
		// } else if ($res_update==1 && $update_id_baru==0 && $update_id_lama==1) {

		// } else if ($res_update==0 && $update_id_baru==1 && $update_id_lama==0) {

		// } else if ($res_update==1 && $update_id_baru==1 && $update_id_lama==1) {
		// 	# code...
		// }

		// if ($res_update==1) {
		// 	if ($update_id_baru==1) {
		// 		if ($update_id_lama==1) {
		// 			1 1 1		
		// 		} else {
		// 			1 1 0
		// 		}
		// 	} else {
		// 		if ($update_id_lama==1) {
		// 			1 0 1
		// 		} else {
		// 			1 0 0
		// 		}
		// 	}
		// } else {
		// 	if ($update_id_baru==1) {
		// 		if ($update_id_lama==1) {
		// 			0 1 1
		// 		} else {
		// 			0 1 0
		// 		}
		// 	} else {
		// 		if ($update_id_lama==1) {
		// 			0 0 1
		// 		} else {
		// 			0 0 0
		// 		}
		// 	} else {
		// 		0 0 0
		// 	}
		// }

		// if($res_update==1 && $update_barang==1){
		// 	echo "berhasil update history dan barang";
		// 	// header( "Location:viewmasuk.php?id=$id&barang=1&history=1" );
		// } else if ($res_update==0 && $update_barang==1) {
		// 	echo "update history gagal dan barang berhasil";
		// 	// header( "Location:viewmasuk.php?id=$id&barang=1&history=0" );
		// } else if ($res_update==1 && $update_barang==0) {
		// 	echo "update history berhasil dan barang gagal";
		// 	// header( "Location:viewmasuk.php?id=$id&barang=0&history=1" );
		// } else {
		// 	echo "update history dan barang gagal";
		// 	// header( "Location:viewmasuk.php?id=$id&barang=0&history=0" );
		// }
	}
}
?>