-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 29 Okt 2016 pada 03.33
-- Versi Server: 5.6.30
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aisen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_barang`
--

CREATE TABLE IF NOT EXISTS `daftar_barang` (
  `id_barang` varchar(30) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `ukuran` varchar(20) NOT NULL,
  `foto` longblob,
  `h_modal` int(20) DEFAULT NULL,
  `h_jual` int(20) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `keterangan` longtext NOT NULL,
  `jumlah` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daftar_barang`
--

INSERT INTO `daftar_barang` (`id_barang`, `id_jenis`, `ukuran`, `foto`, `h_modal`, `h_jual`, `id_supplier`, `keterangan`, `jumlah`) VALUES
('a02', 2, '289', '', 30000, 40000, 2, 'Kode Awal CP002', 500),
('c02', 2, '34', '', 45000, 85000, 1, 'Kode Awal CP002', 470),
('cp29', 2, '29', '', 150000, 250000, 1, '200', 250),
('cp30', 2, '30', '', 150000, 200000, 4, 'cp30 masuk 100', 300),
('cpjg1', 3, '33', '', 123, 321, 2, '', 200),
('d01', 4, 'XL', '', 80000, 120000, 6, 'Kode Awal CP001', 400),
('d02', 3, 'S', '', 35000, 150000, 4, 'Kode Awal KJ009', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_keluar`
--

CREATE TABLE IF NOT EXISTS `history_keluar` (
  `id_keluar` int(11) NOT NULL,
  `id_barang` varchar(30) NOT NULL,
  `ukuran` varchar(20) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_pesan` varchar(30) NOT NULL,
  `tanggal_keluar` varchar(30) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `keterangan` longtext NOT NULL,
  `uplink` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_keluar`
--

INSERT INTO `history_keluar` (`id_keluar`, `id_barang`, `ukuran`, `id_jenis`, `tanggal_pesan`, `tanggal_keluar`, `jumlah`, `keterangan`, `uplink`) VALUES
(7, 'c02', '34', 2, '10/20/2016', '10/24/2016', 30, 'asd', 'Karyawan1'),
(8, 'd01', 'XL', 4, '10/24/2016', '10/29/2016', 60, 'keluar 50 pcs tanggal 29 oktober 2016', 'qweqwe'),
(10, 'd01', 'XL', 4, '10/25/2016', '10/31/2016', 40, 'keluar 40', 'qweqwe'),
(13, 'a02', '289', 2, '10/26/2016', '10/31/2016', 30, 'test', 'qweqwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_masuk`
--

CREATE TABLE IF NOT EXISTS `history_masuk` (
  `id_masuk` int(11) NOT NULL,
  `id_barang` varchar(30) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `ukuran` varchar(30) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `tanggal_masuk` varchar(30) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `keterangan` longtext NOT NULL,
  `uplink` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_masuk`
--

INSERT INTO `history_masuk` (`id_masuk`, `id_barang`, `id_jenis`, `ukuran`, `id_supplier`, `tanggal_masuk`, `jumlah`, `keterangan`, `uplink`) VALUES
(8, 'a02', 2, '28', 2, '10/20/2016', 500, 'Kode Awal CP003', 'Test-Karyawan'),
(10, 'c02', 2, '34', 1, '10/20/2016', 500, 'Kode Awal CP002', 'Test-Karyawan'),
(11, 'd01', 4, '34', 6, '10/20/2016', 500, 'Kode Awal CP001', 'qweqwe'),
(19, 'a02', 2, '34', 1, '10/26/2016', 30, 'tambah 30', 'qweqwe'),
(20, 'cp30', 2, '30', 2, '10/26/2016', 100, '100', 'qweqwe'),
(21, 'cp30', 2, '30', 4, '10/26/2016', 50, 'masuk 50', 'qweqwe'),
(23, 'cpjg1', 3, '33', 2, '10/29/2016', 200, '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_barang`
--

CREATE TABLE IF NOT EXISTS `jenis_barang` (
  `id_jenis` int(11) NOT NULL,
  `jenis_barang` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_barang`
--

INSERT INTO `jenis_barang` (`id_jenis`, `jenis_barang`) VALUES
(1, 'Celana'),
(2, 'Celana Pendek'),
(3, 'Kemeja Jeans'),
(4, 'Celana Panjang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(30) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telepone` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat`, `telepone`) VALUES
(1, 'Levis', 'Jakarta', '0218989884'),
(2, 'Zara', 'Jakarta', '021959448'),
(3, 'Hugo Boss', 'Jakarta', '021-5238490'),
(4, 'Rumah Mode', 'Jakarta', '021213213132'),
(6, 'Giordano', 'Jakarta', '021-5094830');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `hp` varchar(30) NOT NULL,
  `level` enum('Karyawan','Admin') NOT NULL,
  `uplink` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `hp`, `level`, `uplink`) VALUES
(1, 'Master', 'admin', 'Master-Admin', '', 'Admin', ''),
(2, 'Test-Karyawan', 'testkaryawan', 'Test-Karyawan', '', 'Karyawan', ''),
(5, 'Karyawan1', 'karyawan1', 'Karyawan 1', '081298762374', 'Karyawan', 'Master'),
(6, 'qweqwe', 'qweqwe', 'indra', '123', 'Admin', 'Master');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar_barang`
--
ALTER TABLE `daftar_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_jenis` (`id_jenis`,`id_supplier`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `history_keluar`
--
ALTER TABLE `history_keluar`
  ADD PRIMARY KEY (`id_keluar`),
  ADD KEY `id_barang` (`id_barang`,`id_jenis`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indexes for table `history_masuk`
--
ALTER TABLE `history_masuk`
  ADD PRIMARY KEY (`id_masuk`),
  ADD KEY `id_barang` (`id_barang`,`id_jenis`,`id_supplier`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indexes for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history_keluar`
--
ALTER TABLE `history_keluar`
  MODIFY `id_keluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `history_masuk`
--
ALTER TABLE `history_masuk`
  MODIFY `id_masuk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `daftar_barang`
--
ALTER TABLE `daftar_barang`
  ADD CONSTRAINT `daftar_barang_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `daftar_barang_ibfk_2` FOREIGN KEY (`id_jenis`) REFERENCES `jenis_barang` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `history_keluar`
--
ALTER TABLE `history_keluar`
  ADD CONSTRAINT `history_keluar_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `daftar_barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `history_keluar_ibfk_2` FOREIGN KEY (`id_jenis`) REFERENCES `jenis_barang` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `history_masuk`
--
ALTER TABLE `history_masuk`
  ADD CONSTRAINT `history_masuk_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `daftar_barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `history_masuk_ibfk_2` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `history_masuk_ibfk_3` FOREIGN KEY (`id_jenis`) REFERENCES `jenis_barang` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
