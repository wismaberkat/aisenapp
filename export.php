<?php
require_once "api/Helpers/GlobalHelper.php";

$helper = new GlobalHelper();
// Fungsi header dengan mengirimkan raw data excel
// header("Content-type: application/vnd-ms-excel");

$date = date("d-m-Y");
// Mendefinisikan nama file ekspor "hasil-export.xls"
if (isset($_GET["daftar"]) || isset($_GET["historymasuk"]) || isset($_GET["historykeluar"])) {
	
	$daftar = $_GET["daftar"];
	$masuk = $_GET["historymasuk"];
	$keluar = $_GET["historykeluar"];
	if (!empty($daftar)) {
		header("Content-Disposition: attachment; filename=".$daftar."_".$date.".xls");
		// isi table
		include 'api/tabel-barang.php';
	}
	if (!empty($masuk)) {
		header("Content-Disposition: attachment; filename=".$masuk."_".$date.".xls");
		// isi table
		include 'api/tabel-history-masuk.php';
	}
	if (!empty($keluar)){
		header("Content-Disposition: attachment; filename=".$keluar."_".$date.".xls");
		// isi table
		include 'api/tabel-history-keluar.php';
	}	
}
?>