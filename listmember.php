<?php
    session_start();
	
    if(!isset($_SESSION["username"]) || $_SESSION['level'] != 'Admin')
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Karyawan";

    $username = $_SESSION["username"];
    $level = $_SESSION["level"];
    $nWhere = ($_SESSION["level"] == "Admin") ? "" : "WHERE username = '$username'";

    if(isset($_GET['act']) && $_SESSION['level'] == 'Admin')
    {
        $sql = "DELETE from user where id = '".$_GET['karyawan']."'";
        $resdel = $helper->database->query($sql);
        
    }

    $start = 0;
    $limit = 100;

    if(isset($_GET['p']))
    {
        $p = $_GET['p'];
        $start = ($p-1)*$limit;
    }else{$p=1;}

    $condition = ($_SESSION["level"] == "Admin") ? "1" : "username='$username'";
    $members = $helper->database->select("history left join list_product on history.type = list_product.id", "history.id as hisid,*,history.status as stat", $condition, "id DESC");
    $all = "SELECT * FROM user ";
    $sql = "SELECT * FROM user ";
    $users  = $helper->database->query($sql);
    $allusers = $helper->database->query($all);

    /* echo $sql; */

    $jml = $allusers->num_rows;

    $totalpage = ceil($jml/$limit);


    
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
                    <div class="block">
                        <div class="block">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tabel Daftar Karyawan</h3>
                                    <?php if($_SESSION['level'] == 'Admin'):?>
                                    <div class="pull-right"><a href="add-member.php" class="btn btn-primary">Tambah Karyawan</a></div>
                                    <?php endif;?>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                    <!-- <th>ID Karyawan</th> -->
                                                    <th class="rata-tengah">No.</th>
                                                    <th class="rata-tengah">Nama Karyawan</th>
                                                    <th class="rata-tengah">Username</th>
                                                    <th class="rata-tengah">Password</th>
                                                    <th class="rata-tengah">Handphone</th>
                                                    <th class="rata-tengah">Level</th>
                                                    <th class="rata-tengah">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
													$i = 1;
													$total_harga = 0;
													$condition = ($_SESSION["level"] == "Admin") ? "1" : "username='$username'";
													$histories = $helper->database->select("history left join list_product on history.type = list_product.id", "*,history.status as stat,history.id as hisid", $condition, "hisid DESC");
													

                                                    if ($jml > 0) {
                                                        while ($user = $users->fetch_object() ) {
                                                           // print_r($history);
                                                        $html = "<tr>";
                                                        // $html .= "<td>".$user->id."</td>";
                                                        $html .= "<td align='center'>".$i."</td>";
                                                        $html .= "<td align='center'>".$user->nama."</td>";
                                                        $html .= "<td align='center'>".$user->username."</td>";
                                                        $html .= "<td align='center'>".$user->password."</td>";
                                                        $html .= "<td align='center'>".$user->hp."</td>";
                                                        $html .= "<td align='center'>".$user->level."</td>";
                                                        $html .= "<td align='center'><a href=\"".$helper->baseUrl."/editaccount.php?user=$user->id\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$user->id."' >
                                                                    <i class='fa fa-hand-pointer-o'> Ubah </i>
                                                                </button></a>
                                                                <a href=\"".$helper->baseUrl."/listmember.php?act=delete&karyawan=$user->id\"<button type='button' class='btn btn-danger ' data-order-id='".$service->id."' >
                                                                    <i class='fa fa-close'> Hapus </i>
                                                                </button></a></td>";
                                                        $html .= "</tr>";
                                                        
                                                       // $total_harga = $total_harga + $history->harga;
                                                        
                                                        echo $html;
                                                        
                                                        $i++;
                                                        }
                                                    }
												?>
												
                                            </tbody>
                                        </table>
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                    <?php
                                        if($p>1)
                                        {
                                            echo "<li><a href=\"".$helper->baseUrl."/listmember.php?p=".($p-1)."\">«</a></li>";
                                        }

                                        for ($i=1; $i <= $totalpage ; $i++) {
                                            if($i == $p){echo "<li><a href=\"?p=".$i."\" class=\"current\">".$i."</a></li>";} 
                                            else {echo "<li><a href=\"?p=".$i."\">".$i."</a></li>";}
                                        }
                                        if($p!=$totalpage)
                                        {
                                            echo "<li><a href=\"".$helper->baseUrl."/listmember.php?p=".($p+1)."\">»</a></li>";
                                        }
                                    ?>
                                    
                                    
                                    
                                 </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>