<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Edit Barang";
	
    $username = $_SESSION["username"];
    
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Form Edit Barang</h3>
								</div>
								<?php 
									$id = $_GET['id_barang'];
								?>
								<form method="post" action="<?php echo "edit-b.php?id_barang=".$id; ?>">
									<div class="box-body">
									<?php
									$id = $_GET['id_barang'];
									// echo $id;
									$sql = "SELECT * FROM daftar_barang where id_barang='$id'";
    								$res = $helper->database->query($sql);
    								if (!$res) {
    									echo "Barang tidak ada";
    								} else {
    									$barang = $res->fetch_assoc();
    									$ids = $barang['id_barang'];
    									// echo $ids;
    								}
								?>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="id_barang">Kode Barang</label>
													<input type="text" name="id_barang" class="form-control" id="id_barang" value="<?=$barang['id_barang']?>" <?php if(($_SESSION['level'] != 'Admin')){echo "readonly";} ?>>
												</div>
												<div class="form-group">
													<label class="control-label" for="jenis_barang">Jenis</label>
													<select name="jenis" class="form-control">
														<?php 
														$id_jen = $barang['id_jenis'];
														$sql_jenis= "SELECT * FROM jenis_barang";
														$res_jenis = $helper->database->query($sql_jenis);
														if (!$res_jenis) {
															echo "Jenis tidak ada";
														}else{
															if ($res_jenis->num_rows > 0) {
																while ($jenis = $res_jenis->fetch_assoc()) {
																	$id_jenis = $jenis['id_jenis'];
																	$jenis_barang = $jenis['jenis_barang'];
														?>
																	<option value="<?php echo $id_jenis; ?>"<?php if($id_jen==$id_jenis){echo "selected";}else{} ?>><?php echo $jenis_barang; ?></option>
														<?php 
																}//end of while $jenis
																$res_jenis->data_seek(0);
																foreach ($jenis as $jenisnya) {
																		echo $jenisnya['jenis_barang'];
																	}
															}//endif $res_jenis
														}//end of else 
														?>
													</select>
												</div>
												<div class="form-group">
													<label class="control-label" for="ukuran">Ukuran</label>
													<input type="text" name="ukuran" class="form-control" id="ukuran"  value="<?=$barang['ukuran']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="foto">Foto</label>
													<input type="text" name="foto" class="form-control" id="foto"  value="<?=$barang['foto']?>"  />
												</div>
												<?php if($_SESSION['level'] == 'Admin'): ?>
												<div class="form-group">
													<label class="control-label" for="modalb">BP</label>
													<input type="number" name="modalb" class="form-control" id="modalb"  value="<?=$barang['h_modal']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="jualb">Harga Jual</label>
													<input type="number" name="jualb" class="form-control" id="jualb"  value="<?=$barang['h_jual']?>"  />
												</div>
												<?php endif; ?>
												<div class="form-group">
													<label class="control-label" for="level">Supplier</label>
													<select name="supplier" class="form-control">
														<?php 
															$id_supp = $barang['id_supplier'];
															$sql_supplier= "SELECT * FROM supplier";
															$res_supplier = $helper->database->query($sql_supplier);
															if (!$res_supplier) {
																echo "Jenis tidak ada";
															}else{
																if ($res_supplier->num_rows > 0) {
																	while ($supplier = $res_supplier->fetch_assoc()) {
																		$id_supplier = $supplier['id_supplier'];
																		$nama_supplier = $supplier['nama_supplier'];
														?>
																		<option value="<?php echo $id_supplier; ?>"<?php if($id_supp==$id_supplier){echo "selected";}else{} ?>><?php echo $nama_supplier; ?></option>
														<?php 
																	}//end of while $res_supplier
																	$res_supplier->data_seek(0);
																}//endif $res_supplier
															}//end of else 
														?>
													</select>
												</div>
												<div class="form-group">
													<label class="control-label" for="keterangan">Keterangan</label>
													<!-- <input type="text" class="form-control" id="suplier" placeholder="Masukkan Nama Supplier" required /> -->
													<textarea name="keterangan" id="keterangan" class="form-control" cols="20" rows="3" placeholder="Masukkan Keterangan"><?=$barang['keterangan']?></textarea>
												</div>
												<div class="form-group">
													<label class="control-label" for="stok">Stok Barang</label>
													<input type="number" name="stok" class="form-control" id="stok"  value="<?=$barang['jumlah']?>"  />
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right">Change</button>
												</div> 
											</div>
										</div>
									</div>
								</form>								
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
