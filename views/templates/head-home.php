<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700|IM+Fell+Double+Pica:400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/newsite/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="assets/newsite/css/style.css"> <!-- Resource style -->
	<script src="<?= $helper->baseUrl; ?>/assets/newsite/js/modernizr.js"></script> <!-- Modernizr -->
    <script src="<?= $helper->baseUrl; ?>/assets/newsite/js/main.js"></script>
    <script src="<?= $helper->baseUrl; ?>/assets/newsite/js/jquery-2.1.4.js"></script>
  	
	<title>AISEN APP</title>
</head>