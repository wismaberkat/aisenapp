<section class="sidebar">
	<ul class="sidebar-menu">
		<li class="header">MAIN NAVIGATION</li>
		<?php 
			$arrAPlus = array("Admin");
			
			echo '<li '.$helper->isActive("index.php").'><a href="index.php"><i class="fa fa-home"></i> <span>Home</span></a></li>';
			echo '<li '.$helper->isActive("barangin.php").'><a href="barangin.php"><i class="fa fa-arrow-down"></i> <span>Input Barang</span></a></li>';
			echo '<li '.$helper->isActive("barangout.php").'><a href="barangout.php"><i class="fa fa-arrow-up"></i> <span>Output Barang</span></a></li>';
			echo '<li '.$helper->isActive("viewmasuk.php").'><a href="viewmasuk.php"><i class="fa fa-tasks"></i> <span>History Input Barang</span></a></li>';
			echo '<li '.$helper->isActive("viewkeluar.php").'><a href="viewkeluar.php"><i class="fa fa-tasks"></i> <span>History Output Barang</span></a></li>';
			echo '<li '.$helper->isActive("list-barang.php").'><a href="list-barang.php"><i class="fa fa-database"></i> <span>Daftar Barang</span></a></li>';
			echo '<li '.$helper->isActive("search.php").'><a href="search.php"><i class="fa fa-search"></i> <span>Cari Barang</span></a></li>';
			echo '<li '.$helper->isActive("jenis-barang.php").'><a href="jenis-barang.php"><i class="fa fa-tags"></i> <span>Jenis Barang</span></a></li>';
			// echo '<li '.$helper->isActive("history.php").'><a href="history.php"><i class="fa fa-tasks"></i> <span>History + Total</span></a></li>';
			
			if(in_array($_SESSION["level"], $arrAPlus)){
				echo '<li '.$helper->isActive("supplier.php").'><a href="supplier.php"><i class="fa fa-truck"></i> <span>Supplier</span></a></li>';
				// echo '<li '.$helper->isActive("add-member.php").'><a href="add-member.php"><i class="fa fa-user-plus"></i> <span>Register Karyawan</a></li>';
				echo '<li '.$helper->isActive("listmember.php").'><a href="listmember.php"><i class="fa fa-user"></i> <span>Karyawan</a></li>';                            
			}
			// echo '<li '.$helper->isActive("list.php").'><a href="list.php"><i class="fa fa-fax"></i> <span>List Harga</span></a></li>';
			// echo '<li '.$helper->isActive("faq.php").'><a href="faq.php"><i class="fa fa-question-circle"></i> <span>FAQ</span></a></li>';
		?>
	</ul>
</section>