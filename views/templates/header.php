<header class="main-header">
    <a href="index.php" class="logo">
        <img class="aisen-logo" src="assets/img/logo-in.png" alt="">
        <!-- <span id="logo"><?= $helper->title; ?></span> -->
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="messages-menu">
                     <!-- <a href="#"><?= $_SESSION["nama"]; ?></a> -->
                    <!-- <a href="#"><i class="fa fa-money"></i> Saldo : <span id="sisa_saldo"><?= $helper->getProfileData($_SESSION["username"])->saldo ?></span></a> -->
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <span class=""><?= $_SESSION["nama"]; ?>&nbsp;<span class="caret"></span></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
							<p><?= $_SESSION["nama"]; ?></p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
								<a href="account.php" class="btn btn-default btn-flat">My account</a>
                            </div>
                            <div class="pull-right">
								<a href="logout.php" class="btn btn-default btn-flat">Logout</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<script>
function myFunction() {
    document.getElementById("logo").innerHTML = "W";
}
</script>