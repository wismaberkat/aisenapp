<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- change the color of header and address bar -->
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#357ca5">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#f26904">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#f26904">

	<title><?= $helper->title; ?> - <?= $pageTitle; ?> </title>

	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/plugins/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/plugins/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/site/css/skins/skin-blue.min.css" />
	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/site/css/AdminLTE.min.css" />
	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/plugins/webfont/webfont.css" />
	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/site/css/site.css" />
	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/site/css/styles.css" />
	<link rel="stylesheet" href="<?= $helper->baseUrl; ?>/assets/plugins/jquery-ui/jquery-ui.css" />

</head>
<div id="modal-loading" class="modal"></div>