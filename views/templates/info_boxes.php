<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-light-blue"><i class="fa fa-user"></i></span>
            <div class="info-box-content">
				<span class="info-box-text">NAMA</span>
				<span class="info-box-number"><?= $_SESSION["nama"]; ?></span>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-light-blue"><i class="fa fa-key"></i></span>
            <div class="info-box-content">
				<span class="info-box-text">LEVEL</span>
				<span class="info-box-number"><?= $_SESSION["level"]; ?></span>
            </div>
        </div>
    </div>
    <?php
        $date_now = $helper->date;
        $condition = "tanggal_masuk='$date_now'";
        $transaksi = $helper->database->select("history_masuk", "*","1","id_masuk ASC");
        if ($transaksi) {
            // echo "ada hasil query";
            foreach($transaksi as $masuk)
            {
                // $total_masuk = $total_masuk + $masuk->jumlah;
                $jumlah_masuk = $masuk->jumlah;
                $submit_masuk = $masuk->uplink;
            }
        } else {
            // echo "kosong";
            $jumlah_masuk = "Belum Ada Barang Masuk Hari Ini.";
        }
        
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-up"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Barang Masuk Terakhir</span>
                <span class="info-box-text"><b><?= $submit_masuk; ?></b></span>
                <span class="info-box-number" id="masuk_now"><?= $jumlah_masuk; ?></span>
            </div>
        </div>
    </div>
    <?php
        // $condition2 = "tanggal_pesan='$date_now'";
        // echo $date_now;
        $transaksi2 = $helper->database->select("history_keluar", "*","1","id_keluar ASC");
        if ($transaksi2) {
            // echo "ada hasil query";
            foreach($transaksi2 as $keluar)
            {
                $total_keluar = $keluar->jumlah;
                $submit_keluar = $keluar->uplink;
            }
        } else {
            //echo "ga ada hasil query"; 
            $total_keluar = "Belum ada barang keluar hari ini.";
        }
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-down"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Barang Keluar Terakhir</span>
                <span class="info-box-text"><b><?= $submit_keluar; ?></b></span>
                <span class="info-box-number" id="keluar_now"><?= $total_keluar; ?></span>
            </div>
        </div>
    </div>
    <?php
    $barang = $helper->database->select("daftar_barang", "*","1");                                           
    foreach($barang as $stock_barang)
    {
        $total_stock = $total_stock + $stock_barang->jumlah;
    }
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-light-blue"><i class="fa fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Sisa Barang Total</span>
            <span class="info-box-number" id="total_stock"><a href="<?= $helper->baseUrl; ?>/list-barang.php"><?= $total_stock; ?></a></span>
            </div>
        </div>
    </div>
</div>