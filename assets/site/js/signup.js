var fn_checkAvailEmail= function(pEmail){
    return $.ajax({
        type: "POST",
        url: 'data.php?pAct=CekEmail',
        data:{
           pEmail : pEmail 
        },
        cache: false,
        async: false
    }).responseText;
}

var fn_checkAvailUsername= function(pUsername){
    return $.ajax({
        type: "POST",
        url: 'data.php?pAct=CekUser',
        data:{
           pUsername : pUsername 
        },
        cache: false,
        async: false
    }).responseText;
}

function validateEmailType(pEmail){
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( pEmail );    
}

function cekError(){
    var vError = '';
	
    $('#errorAlert').each(function(){
        vError = vError+$(this).html();
    });
    
    if(vError.length > 0 && $('#errorAlert').html() != "Silakan lengkapi data dibawah ini"){
        $('#errorAlert')
            .removeClass('alert-info')
            .addClass('alert-danger');
    }else{
        $('#errorAlert')
            .removeClass('alert-danger')
            .addClass('alert-info');
    }
}

(function() {
    var password = document.getElementById('password');
    var cpassword = document.getElementById('cpassword');
    var form = document.getElementById('signupform');
    
    var arrHari = [];
    arrHari[1] = 31; arrHari[2] = 29; arrHari[3] = 31; arrHari[4] = 30; arrHari[5] = 31; arrHari[6] = 30;
    arrHari[7] = 31; arrHari[8] = 31; arrHari[9] = 30; arrHari[10] = 31; arrHari[11] = 30; arrHari[12] = 31;    
    
    var pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;    
    
    var checkPasswordValidity = function() {
        if (password.value != cpassword.value && cpassword.value) {
            $('#errorAlert').html('Password tidak cocok');         
            cekError();
            return false;
        }else if (!password.value.match(pattern)) {
            $('#errorAlert').html('Harus mengisi minimal 8 karakter, dengan huruf besar, huruf kecil, angka dan special karakter');
            cekError();
            return false;
        }else {            
            $('#errorAlert').html('Silakan lengkapi data dibawah ini');
            cekError();
            return true;
        }        
    };
    
    var checkNoValidity = function() {
        if (!$('#txt_hp').val()) {                        
            $('#errorAlert').html('No HP / WhatsApp / Telegram harus diisi.');
            cekError();
            return false;
        }else {            
            $('#errorAlert').html('Silakan lengkapi data dibawah ini');
            cekError();
            return true;
        }        
    };
       
    $("#signupform").submit(function(event) {
        if (!checkPasswordValidity()) {
            event.preventDefault();
            password.focus();
			$("#errorAlert").parent().show();
        }else if($('#errorAlert').html().length > 0 && $('#errorAlert').html() != "Silakan lengkapi data dibawah ini"){
            event.preventDefault();
			$("#errorAlert").parent().show();            
            $('#errorAlert').html('Error validasi data, mohon cek ulang');
        }else if(!$('#username').val()){
            event.preventDefault();
            $('#username').focus();
			$("#errorAlert").parent().show();
            $('#errorAlert').html('Username tidak boleh kosong');            
        }else if(!$('#captcha-form').val()){
            event.preventDefault();
            $('#captcha-form').focus();  
			$("#errorAlert").parent().show();
            $('#errorAlert').html('Captcha tidak boleh kosong');
        }else if(!fn_checkAvailEmail($('#txt_email').val())){
            event.preventDefault();
            $('#txt_email').focus();
			$("#errorAlert").parent().show();
            $('#errorAlert').html('Email '+$('#txt_email').val()+' sudah terdaftar, mohon gunakan email lain');
        }else if(!fn_checkAvailUsername($('#username').val())){
            event.preventDefault();
            $('#username').focus();  
			$("#errorAlert").parent().show();
            $('#errorAlert').html('Username '+$('#username').val()+' sudah terdaftar, mohon gunakan user lain');
        }
        cekError();
    });

// ======== EMAIL VALIDATE ==========
    $('input[type=email]').typing({
        stop: function (event, $elem) {
            var val = validateEmailType($elem.val());
			
            if(!val)
                $('#errorAlert').html('Email tidak valid.');
			else
				$('#errorAlert').html('Silakan lengkapi data dibawah ini');
			
			cekError();
        }, delay: 800
    });    

    $('#password, #cpassword').typing({
        stop: function (event, $elem) {
            checkPasswordValidity();
        }, delay: 800
    });  
    
    $('#txt_hp').typing({
        stop: function (event, $elem) {
            checkNoValidity();
        }, delay: 800
    });
    // ========== END TYPING============
    
    
    // ========== MASKED INPUT ============
	
    cekError();
}());

