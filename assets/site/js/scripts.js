$(function() {
	$(document).bind("ajaxSend", function(){
		$(document.body).addClass("loading");
	}).bind("ajaxComplete", function(){
		$(document.body).removeClass("loading");
	}).bind("ready", function(){
		setTimeout(function() { $(document.body).removeClass("loading"); }, 1000);
	});
});