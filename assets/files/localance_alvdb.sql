-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20 Mar 2016 pada 17.46
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alvdb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT 'member',
  `pembeli` varchar(50) NOT NULL,
  `type` varchar(100) NOT NULL,
  `data` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('Gagal','Proses','Sukses','') NOT NULL,
  `nomor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `list`
--

CREATE TABLE `list` (
  `id` int(11) NOT NULL,
  `server` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `rate` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `status` enum('Ready','Not Ready','','') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `list`
--

INSERT INTO `list` (`id`, `server`, `rate`, `min`, `max`, `status`) VALUES
(51, 'Instagram Followers (Speed 1-10k / Day, Unlimited, Send username only)', 35000, 100, 1000000, 'Ready'),
(100, 'Instagram Followers (Instant, Max 5.000)', 25000, 20, 5000, 'Ready'),
(48, 'Instagram Followers REAL HUMAN Worldwide/International (Fast)', 120000, 100, 7500, 'Ready'),
(28, 'Instagram Arab Followers (Unlimited, Send username only)', 85000, 100, 100000, 'Ready'),
(15, 'Instagram Likes (HQ, Instant)', 25000, 10, 5000, 'Ready'),
(52, 'Instagram Likes (Speed 1-10k / Day, Unlimited)', 35000, 20, 5000, 'Ready'),
(5, 'Instagram Likes (Fast)', 15000, 100, 1000000, 'Ready'),
(54, 'Instagram Likes REAL HUMAN (Active users)', 170000, 20, 10000, 'Ready'),
(50, 'Instagram Arab Likes - [Rp. 17.000 / 100]', 170000, 100, 1000000, 'Ready'),
(4, 'Facebook Fanpage Likes (No Musical Page)', 160000, 250, 1000000, 'Ready'),
(9, 'Facebook Middle East Fanpage Likes (100-300 Likes/day, REAL & ACTIVE, Start in 6 Hours)', 160000, 200, 1000000, 'Ready'),
(104, 'Facebook Middle East Fanpage Likes (FASTER: 1k-3k Likes/day, REAL & ACTIVE, Instant)', 185000, 500, 700000, 'Ready'),
(6, 'Facebook English Speaking Fanpage Likes (50-100 Likes/day, All Types Accepted, Non Arabic, REAL & ACTIVE, Instant)', 275000, 200, 1000000, 'Ready'),
(96, 'Facebook English Speaking Fanpage Likes (FASTER: 1k-3k Likes/day, No Accept Musical/Band Type)', 315000, 2000, 5000000, 'Ready'),
(95, 'Facebook USA Fanpage Likes (1000% Real Human USA, Legit Ads Method, Instant)', 1260000, 500, 1000000, 'Not Ready'),
(85, 'Facebook Group Members', 30000, 100, 15000, 'Ready'),
(12, 'Facebook REAL Group Members', 45000, 10000, 1000000, 'Ready'),
(7, 'Facebook REAL Post Likes (South/North American, Max 25k/Post, Instant)', 210000, 100, 25000, 'Ready'),
(80, 'Facebook Photo/Post Likes (HQ, Instant)', 30000, 100, 9400, 'Ready'),
(23, 'Facebook Website Likes', 140000, 100, 15000, 'Ready'),
(81, 'Facebook REAL Followers', 840000, 500, 20000, 'Not Ready'),
(18, 'Facebook Video Views (Speed 100k/Day, Order kelipatan 5.000)', 12000, 5000, 1000000, 'Ready'),
(59, 'Facebook REAL Friends', 735000, 300, 5000, 'Ready'),
(60, 'Facebook REAL Event Attendees', 2275000, 100, 3000, 'Not Ready'),
(61, 'Facebook REAL Reviews (5 Stars Rating)', 3500000, 25, 5000, 'Not Ready'),
(8, 'Facebook Post Share (Speed 1 Million/Day, Working with Fanpage Post only / NO Profile Post)', 60000, 1000, 1000000, 'Ready'),
(39, 'Facebook REAL Post Share', 630000, 100, 5000, 'Ready'),
(20, 'Twitter Followers (HQ, Unlimited, Fast, Send username only)', 10000, 20, 1000000, 'Not Ready'),
(27, 'Twitter Arab Followers (Instant, Send username only)', 40000, 100, 200000, 'Ready'),
(34, 'Twitter Indian Followers (Instant, Send username only)', 70000, 100, 200000, 'Ready'),
(33, 'Twitter USA Followers (Instant, Send username only)', 55000, 100, 100000, 'Ready'),
(1, 'Twitter UK Followers (Instant, Send username only)', 55000, 100, 10000, 'Ready'),
(83, 'Twitter Retweets (HQ, Unlimited, Fast)', 12000, 100, 1000000, 'Ready'),
(86, 'Twitter USA Retweets (Instant)', 55000, 20, 1000000, 'Ready'),
(40, 'Twitter Arab Retweets (Instant)', 70000, 100, 100000, 'Ready'),
(2, 'Twitter India Retweets (Instant)', 70000, 100, 100000, 'Ready'),
(84, 'Twitter Favourites (HQ, Unlimited, Fast)', 10000, 20, 1000000, 'Ready'),
(35, 'Twitter USA Favourites (Instant)', 55000, 100, 100000, 'Ready'),
(71, 'Twitter Arab Favourites (Instant)', 70000, 100, 100000, 'Ready'),
(3, 'Twitter India Favourites (Instant)', 70000, 100, 100000, 'Ready'),
(77, 'Youtube Worldwide Views', 25000, 1000, 1000000, 'Ready'),
(98, 'Youtube Worldwide Views (Speed 10k-20k/day)', 15000, 1000, 1000000, 'Ready'),
(74, 'Youtube Likes', 210000, 100, 10000, 'Ready'),
(19, 'Youtube Dislikes', 210000, 100, 10000, 'Ready'),
(70, 'Youtube Real Subscribers (NO DROP, Speed 20-50 Subs/day)', 2100000, 100, 3000, 'Ready'),
(105, 'Youtube Favourites', 225000, 100, 50000, 'Ready'),
(106, 'Youtube Reshare', 85000, 50, 1000000, 'Ready'),
(24, 'SoundCloud Plays (Instant)', 6000, 100, 1000000, 'Ready'),
(25, 'SoundCloud Downloads (Instant)', 6000, 100, 100000, 'Ready'),
(72, 'SoundCloud Likes (Instant) - [Rp. 14.000 / 100]', 140000, 20, 1000000, 'Ready'),
(32, 'SoundCloud Followers (Instant, Send username only)', 140000, 20, 1000000, 'Ready'),
(47, 'SoundCloud Comment (Instant)', 315000, 20, 1000000, 'Ready'),
(88, 'Google Plus Likes', 225000, 10, 5000, 'Ready'),
(89, 'Google Plus Reshare', 280000, 10, 5000, 'Ready'),
(90, 'Google Plus Followers', 420000, 100, 1000, 'Ready'),
(45, 'Vine Followers (HQ, Instant) **require Vine user ID**', 17000, 20, 200000, 'Ready'),
(44, 'Vine Likes (HQ, Instant)', 17000, 20, 200000, 'Ready'),
(43, 'Vine Revine (HQ, Instant)', 17000, 20, 200000, 'Ready'),
(99, 'Vine Loops (HQ, Instant)', 17000, 20, 1000000, 'Ready');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tickets`
--

CREATE TABLE `tickets` (
  `id` int(1) NOT NULL,
  `parent_id` int(1) NOT NULL DEFAULT '0',
  `isread` int(1) NOT NULL DEFAULT '0' COMMENT '0=unread,1=read',
  `no_hp` varchar(20) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `file_path` varchar(100) DEFAULT NULL,
  `message` text NOT NULL,
  `user_create` varchar(30) NOT NULL,
  `date_create` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0=new/pending,1=read,2=responsed,3=Waiting Reply,4=close'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `hp` varchar(30) DEFAULT NULL,
  `id_line` varchar(80) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('Member','Reseller','Admin','') NOT NULL,
  `saldo` int(11) NOT NULL,
  `gunakan` int(11) NOT NULL,
  `uplink` varchar(30) NOT NULL,
  `key` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `hp`, `id_line`, `username`, `password`, `level`, `saldo`, `gunakan`, `uplink`, `key`) VALUES
(1, 'Alvon Timotius', NULL, NULL, NULL, 'admin', 'admin', 'Admin', 45400, 567100, 'Master Account', 'b81cbe9b6b3d5c270ea6572c4ec8b34b'),
(5, 'Member Account', 'member@gmail.com', '+6287854750809', NULL, 'member', 'member', 'Member', 0, 0, 'alvon', ''),
(3, 'Reseller Account', NULL, NULL, NULL, 'reseller', 'reseller', 'Reseller', 0, 0, 'alvon', ''),
(69, 'demo', 'demo@demo.com', '081234567890', 'demo', 'demo_admin', 'demo_admin', 'Admin', 0, 0, 'Master Account', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `list`
--
ALTER TABLE `list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
