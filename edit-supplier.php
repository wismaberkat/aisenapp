<?php
    session_start();
	
    if(!isset($_SESSION["username"]) || $_SESSION['level'] != 'Admin')
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Edit Supplier";
	
    $username = $_SESSION["username"];
    
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Form Edit Supplier</h3>
								</div>
								<?php 
									$id = $_GET['id_supplier'];
								?>
								<form method="post" action="<?php echo "edit-s.php?id_supplier=".$id; ?>">
									<div class="box-body">
									<?php
									$id = $_GET['id_supplier'];
									// echo $id;
									$sql = "SELECT * FROM supplier where id_supplier='$id'";
    								$res = $helper->database->query($sql);
    								if (!$res) {
    									echo "Supplier Tidak";
    								} else {
    									$supp = $res->fetch_assoc();
    									$ids = $supp['id_supplier'];
    									// echo $ids;
    								}
								?>
										<div class="row">
											<div class="col-md-12">
												<input type="hidden" name="id_supplier" class="form-control" id="id_supplier" value="<?=$supp['id_supplier']?>" readonly>
												<div class="form-group">
													<label class="control-label" for="nama_supplier">Nama Supplier</label>
													<input type="text" name="nama_supplier" class="form-control" id="nama_supplier"  value="<?=$supp['nama_supplier']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="alamat">Alamat</label>
													<input type="text" name="alamat" class="form-control" id="alamat"  value="<?=$supp['alamat']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="telepone">Telepon</label>
													<input type="text" name="telepone" class="form-control" id="telepone"  value="<?=$supp['telepone']?>"  />
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right"> Update </button>
												</div> 
											</div>
										</div>
									</div>
								</form>								
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>