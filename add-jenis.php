<?php
    session_start();
	
    if(!isset($_SESSION["username"]) || ($_SESSION["level"] != "Admin")) {
    	session_destroy();
        header("location:login.php");
    }
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Tambah Jenis Barang";
	
    $username = $_SESSION["username"];
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Tambah Jenis Barang</h3>
								</div>
								<form method="post" action="">
									<div class="box-body">
									<?php
									
									if(isset($_POST['jenis_barang'])):
									
									// $id = trim($_POST['id_jenis']);
									$jenis = trim($_POST['jenis_barang']);

									// $sql = "INSERT INTO list_product(id,server,rate,min,max,status) VALUES('$prod_id','$nama','$rate','$min','$max','$status')";
									$sql = "INSERT INTO jenis_barang(jenis_barang) VALUES ('$jenis')";
									
									$res = $helper->database->query($sql);

									if($res == 1)
									{
										echo '<div class="alert alert-success alert-dismissible" style="display:block;">
                								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                								  <h4><i class="icon fa fa-check"></i> Sukses !</h4>
                								  Jenis Barang Berhasil Ditambahkan.
              									  </div>';
									}else{
										echo '<div class="alert alert-danger alert-dismissible" style="display:block;">
                								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                								  <h4><i class="icon fa fa-close"></i> Gagal !</h4>
                								  Gagal Menambahkan Jenis Barang.
              									  </div>';

									}
									endif;
									?>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="jenis_barang">Nama Jenis Barang</label>
													<input type="text" name="jenis_barang" class="form-control" id="jenis_barang" placeholder="Masukkan Jenis Barang" value=""/>
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right">Add Jenis Barang</button>
												</div> 
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<?php 
// 	//1
// 	if (idsama && jenissama && ukuran) {
// 		update
// 	} else {
// 		create new
// 	}
// 	//2
// 	if (idsama) {
// 		if (jenissama) {
// 			if (ukuransama) {
// 				"barang berhasil di-update"
// 			}else{
// 				"error! tidak dapat memasukkan kode barang dan jenis yang sama dengan ukuran berbeda.
// 				silahkan buat kode barang baru untuk memasukkan ukuran yang berbeda"
// 			}
// 		}else{
// 			"error! tidak dapat memasukkan jenis barang berbeda dengan id yang sama.
// 			silahkan buat kode barang baru dengan jenis yang mau dipilih"
// 		}
// 	} else {
// 		"barang berhasil di input. "
// 	}
// 10025C15
// 10025C16
?>
