<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/Globalhelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "History Barang Masuk";

    //delete history
    if (isset($_GET["act"]) && isset($_GET["masuk"]) && isset($_GET["barang"])) {
        // $delete = $_GET["act"];
        $id_masuk = $_GET["masuk"];
        $id_barang = $_GET["barang"];
        $res_stok = $helper->database->select("daftar_barang","jumlah","id_barang='$id_barang'");
        foreach($res_stok as $stok){
            $stok_awal = $stok->jumlah;
        }
        $res_masuk = $helper->database->select("history_masuk","jumlah","id_masuk='$id_masuk'");
        if (!$res_masuk) {
            header("location:viewmasuk.php");   
        } else {
            foreach($res_masuk as $masuk){
                $stok_masuk = $masuk->jumlah;
            }
        }
        $stok_akhir = $stok_awal - $stok_masuk;
        $update_stok = array(
            array("jumlah",$stok_akhir)
        );
        $update = $helper->database->update("daftar_barang",$update_stok,"id_barang='$id_barang'");
        $delete = $helper->database->delete("history_masuk","id_masuk='$id_masuk'");
        // $delete = true;
        if (!$delete) {
            $notif = "Gagal menghapus history";
            header("location:viewmasuk.php?deleted=0&notif=$notif");
        } else {
            $notif = "Sukses menghapus history";
            header("location:viewmasuk.php?deleted=1&notif=$notif");
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
                <?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?><small>Daftar barang yang masuk</small></h1>
                </section>
                <!-- <section class="content"> -->
                    <?php 
                    if (isset($_GET["deleted"]) && isset($_GET["notif"])) {
                        $isDelete = $_GET["deleted"];
                        if ($isDelete==1) {
                            echo '<div class="alert alert-success alert-dismissible notif" style="display:block;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h4><i class="icon fa fa-check"></i> Sukses ! </h4>
                                                    '.$_GET["notif"].'
                                                    </div>';
                        } else {
                            echo '<div class="alert alert-danger alert-dismissible notif" style="display:block;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h4><i class="icon fa fa-close"></i> Gagal ! </h4>
                                                    '.$_GET["notif"].'
                                                    </div>';
                        }
                    }
                    if (isset($_GET["updated"]) && isset($_GET["notif"])) {
                        $isUpdate = $_GET["updated"];
                        if ($isUpdate==1) {
                            echo '<div class="alert alert-success alert-dismissible notif" style="display:block;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h4><i class="icon fa fa-check"></i> Sukses ! </h4>
                                                    '.$_GET["notif"].'
                                                    </div>';
                        } else {
                            echo '<div class="alert alert-danger alert-dismissible notif" style="display:block;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h4><i class="icon fa fa-close"></i> Gagal ! </h4>
                                                    '.$_GET["notif"].'
                                                    </div>';
                        }
                    }
                    ?>
                <!-- </section> -->
                <section class="content">
                    <div class="block">
                        <div class="block">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tabel History Barang Masuk</h3>
                                    <form action="hasil_search_masuk.php" method="post">
                                        <div class="col-md-4" style="float:right;">
                                            <div class="input-group">
                                                <input type="text" name="keyword" class="form-control" placeholder="Masukkan Kode Barang/Jenis Barang" required>
                                                <span class="input-group-btn">
                                                    <input type="submit" class="btn btn-default" value="Cari">
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                    <th class="rata-tengah">No</th>
                                                    <th class="rata-tengah">Tanggal Masuk</th>
                                                    <th class="rata-tengah">Kode Barang</th>
                                                    <th class="rata-tengah">Jenis Barang</th>
                                                    <th class="rata-tengah">Ukuran</th>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <th class="rata-tengah">Supplier</th>
                                                    <?php endif; ?>
                                                    <th class="rata-tengah">Keterangan</th>
                                                    <th class="rata-tengah">Jumlah Masuk</th>
                                                    <th class="rata-tengah">Submitted By</th>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <th class="rata-tengah">Action</th>
                                                    <?php endif; ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                // echo $id_masuk;
                                                echo $stok_akhir;
                                                    $i = 1;
                                                    $transaksi = $helper->database->select("history_masuk", "*","1","id_masuk DESC");
                                                    foreach($transaksi as $masuk)
                                                    {
                                                        $html = "<tr>";
                                                        $html .= "<td align='right'>".$i."</td>";
                                                        $html .= "<td align='center'>".$masuk->tanggal_masuk."</td>";
                                                        $html .= "<td align='center'>".$masuk->id_barang."</td>";

                                                        //get jenis_barang
                                                        $idjenis=$masuk->id_jenis;
                                                        $j_condition = "id_jenis='$idjenis'";
                                                        $jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);
                                                        foreach ($jenis as $jenis_barang) {
                                                            $html .= "<td align='center'>".$jenis_barang->jenis_barang."</td>";
                                                        }
                                                        $html .= "<td align='center'>".$masuk->ukuran."</td>";

                                                        //get nama supplier
                                                        $id_supplier=$masuk->id_supplier;
                                                        $s_condition = "id_supplier='$id_supplier'";
                                                        $supplier = $helper->database->select("supplier", "nama_supplier",$s_condition);
                                                        foreach ($supplier as $supp) {
                                                            if($_SESSION['level'] == 'Admin'):
                                                            $html .= "<td align='center'>".$supp->nama_supplier."</td>";
                                                            endif;
                                                        }

                                                        $html .= "<td align='center'>".$masuk->keterangan."</td>";
                                                        $html .= "<td align='right'>".$masuk->jumlah."</td>";
                                                        $html .= "<td align='center'>".$masuk->uplink."</td>";
                                                        if($_SESSION['level'] == 'Admin'):
                                                        $html .= "<td align='center'><a href=\"".$helper->baseUrl."/edit-masuk.php?id=$masuk->id_masuk\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$user->id."' >
                                                                        <i class='fa fa-hand-pointer-o'></i>
                                                                    </button></a>
                                                                    <a href=\"".$helper->baseUrl."/viewmasuk.php?act=delete&masuk=$masuk->id_masuk&barang=$masuk->id_barang\"<button type='button' class='btn btn-danger ' data-order-id='".$service->id."' >
                                                                        <i class='fa fa-close'></i>
                                                                    </button></a></td>";
                                                        endif;
                                                        $html .= "</tr>";
                                                        
                                                        $total_masuk = $total_masuk + $masuk->jumlah;
                                                        
                                                        echo $html;
                                                        
                                                        $i++;
                                                    }
                                                ?>
                                                <tr class="bg-success">
                                                    <td colspan="2"></td>
                                                    <td colspan="2"></td>
                                                    <td colspan="1"></td>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <td colspan="1"></td>
                                                    <?php endif; ?>
                                                    <td colspan="1" align="center"><b>Total Barang masuk</b></td>
                                                    <td colspan="1" align="right"><b><?= $total_masuk; ?></b></td>
                                                    <td colspan="1"></td>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <td colspan="1"></td>
                                                    <?php endif; ?>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <p><a href="export.php?historymasuk=<?php echo $pageTitle?>" class="export"><button class="btn btn-info">Export Data to Excel</button></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>