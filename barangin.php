<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Barang Masuk";
?>

<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?> <small>Masukkan detail barang yang masuk.</small></h1>
                </section>
                <section class="content">
                	<div class="row">
                    	<div class="block">
                            <div class="col-xs-12">
                        	<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tabel Daftar Barang</h3>
                                    <div class="col-md-4" style="float:right;">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Masukkan Kode Barang/Jenis Barang">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Search</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                	<th class="rata-tengah">NO</th>
                                                	<th class="rata-tengah">Kode Barang</th>
                                                	<th class="rata-tengah">Jenis Barang</th>
                                                	<th class="rata-tengah">Ukuran</th>
                                                	<th class="rata-tengah">Foto</th>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                	<th class="rata-tengah">BP</th>
                                                    <?php endif; ?>
                                                	<th class="rata-tengah">Harga Jual</th>
                                                	<th class="rata-tengah">Supplier</th>
                                                	<th class="rata-tengah">Keterangan</th>
                                                	<th class="rata-tengah">Stok</th>
                                                	
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php
    													$i = 1;
                                                        $barang = $helper->database->select("daftar_barang", "*","1");											
    													foreach($barang as $stock_barang)
    													{
                                                            $jumlah_awal = $stock_barang->jumlah;
                                                            // echo $jumlah_awal;
                                                            //get jenis_barang
                                                            $id_jenis = $stock_barang->id_jenis;
                                                            $j_condition = "id_jenis='$id_jenis'";
                                                            $jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);
                                                            // echo $jenis;

                                                            $html = "<tr>";
                                                            $html .= "<td align='center'>".$i."</td>";
                                                            $html .= "<td align='center'>".$stock_barang->id_barang."</td>";

                                                            foreach($jenis as $jenis_barang)
                                                            {
                                                                $html .= "<td align='center'>".$jenis_barang->jenis_barang."</td>";
                                                            }
                                                            $html .= "<td align='center'>".$stock_barang->ukuran."</td>";
    														$html .= "<td align='center'>".$stock_barang->foto."</td>";
                                                            if($_SESSION['level'] == 'Admin'):
                                                            $html .= "<td align='right'>".$helper->format_rupiah($stock_barang->h_modal)."</td>";
                                                            endif;
                                                            $html .= "<td align='right'>".$helper->format_rupiah($stock_barang->h_jual)."</td>";
                                                            //get nama_supplier
                                                            $id_supplier = $stock_barang->id_supplier;
                                                            $s_condition = "id_supplier='$id_supplier'";
                                                            $supp = $helper->database->select("supplier", "nama_supplier",$s_condition);
                                                            foreach($supp as $supplier)
                                                            {
                                                                $html .= "<td align='center'>".$supplier->nama_supplier."</td>";
                                                            }
                                                            $html .= "<td align='center'>".$stock_barang->keterangan."</td>";
                                                            $html .= "<td align='right'>".$stock_barang->jumlah."</td>";
                                                            // $html .= "<td><a href=\"".$helper->baseUrl."/edit-barang.php?id_barang=$stock_barang->id_barang\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$user->id."' >
                                                            //             <i class='fa fa-hand-pointer-o'></i>
                                                            //         </button></a></td>";
    														$html .= "</tr>";
    														$html .='<input type="hidden" id="jumlah_awal" value="'.$stock_barang->jumlah.'">';
    														echo $html;
    														
    														$i++;
    													}
    												?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                	</div>
                	<div class="alert alert-danger alert-dismissable">
                		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                		<p id="errorAlert">&nbsp;</p>
                	</div>
                	<div class="row">
                        <div class="col-xs-12 col-sm-7">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Form Barang Masuk</h3>
                                </div>
								<!-- <form id="barangMasuk"> -->
                               <form id="barangMasuk" action="api/barang-masuk1.php" method="post" enctype="multipart/form-data" accept="image/jpg">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
												<div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="link">Jenis Barang :</label>
                                                            <select id="jenisbarang" name="jenis" class="form-control select2" required>
                                                            	<option value="">-- Pilih Jenis Barang --</option>
																<?php 
																$sql_jenis= "SELECT * FROM jenis_barang";
																$res_jenis = $helper->database->query($sql_jenis);
																if (!$res_jenis) {
																	echo "Jenis tidak ada";
																}else{
																	if ($res_jenis->num_rows > 0) {
																		while ($jenis = $res_jenis->fetch_assoc()) {
																			$id_jenis = $jenis['id_jenis'];
																			$jenis_barang = $jenis['jenis_barang'];
																?>
																			<option value="<?php echo $id_jenis; ?>"><?php echo $jenis_barang; ?></option>
																<?php 
																		}//end of while $jenis
																		$res_jenis->data_seek(0);
																	}//endif $res_jenis
																}//end of else 
																?>
                                                            </select>
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="barang">Kode Barang :</label>
                                                            <input class="form-control" name="barang" id="idbarang" placeholder="Masukkan Kode Barang" type="text" required>
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="ukuran">Ukuran :</label>
                                                             <input class="form-control" name="ukuran" id="ukuran" placeholder="Masukkan Ukuran" type="text" required>
                                                        </div>
														<div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="supplier">Supplier :</label>
                                                            <select id="supplier" name="supp" class="form-control select2" required>
                                                            	<option value="">-- Pilih Supplier --</option>
																<?php 
																	$sql_supplier= "SELECT * FROM supplier";
																	$res_supplier = $helper->database->query($sql_supplier);
																	if (!$res_supplier) {
																		echo "Jenis tidak ada";
																	}else{
																		if ($res_supplier->num_rows > 0) {
																			while ($supplier = $res_supplier->fetch_assoc()) {
																				$id_supplier = $supplier['id_supplier'];
																				$nama_supplier = $supplier['nama_supplier'];
																?>
																				<option value="<?php echo $id_supplier; ?>"><?php echo $nama_supplier; ?></option>
																<?php 
																			}//end of while $res_supplier
																			$res_supplier->data_seek(0);
																		}//endif $res_supplier
																	}//end of else 
																?>
                                                            </select>
                                                            <!-- <input type="text" class="form-control" id="suplier" placeholder="Masukkan Nama Supplier" required /> -->
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="foto">Foto :</label>
                                                            
                                                            <input id="foto" name="photo" type="file" class="file">
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
													<?php 
														$arrAPlus = array("Admin");
														if(in_array($_SESSION["level"], $arrAPlus)){
													?>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="control-label" for="modalb">BP :</label>
                                                            <input class="form-control" name="modal" id="modalb" placeholder="Masukkan BP" type="number" required>
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
                                                    <?php 
                                                    	}
                                                    ?>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="control-label" for="jualb">Jual :</label>
                                                            <input class="form-control" name="jual" id="jualb" placeholder="Masukkan Harga Jual" type="number" required>
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="jumlah">Jumlah Masuk :</label>
                                                            <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Masukkan Jumlah" required>
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="supplier">Keterangan :</label>
                                                            <!-- <input type="text" class="form-control" id="suplier" placeholder="Masukkan Nama Supplier" required /> -->
                                                            <textarea name="ket" name="ket" id="keterangan" class="form-control" cols="20" rows="3" placeholder="Masukkan Keterangan"></textarea>
                                                        </div>
                                                        <div class="margin-bottom hidden-xs"></div>
                                                    </div>
												</div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <!-- <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit"> -->
                                                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
										</div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Result!</h3>
                                </div>
								<div class="box-body" id="barangResult">
								<?php 

                                if (isset($_GET["masuk"]) && isset($_GET["transaksi"])) {
                                    $sukses = $_GET["masuk"];
                                    $transaksi = $_GET["transaksi"];
                                    if ($sukses==0) {
                                    	if ($transaksi==0) {
	                                        echo '<div class="alert alert-danger alert-dismissible" style="display:block;">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                                            <h4><i class="icon fa fa-close"></i> Gagal!</h4>
	                                            Input Barang Gagal! Transaksi tidak tercatat. 
	                                            </div>';
                                    	} else {
                                    		echo '<div class="alert alert-danger alert-dismissible" style="display:block;">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                                            <h4><i class="icon fa fa-close"></i> Gagal!</h4>
	                                            Input Barang Gagal! transaksi tercatat.
	                                            </div>';
                                    	}
                                    } else {
                                    	if ($transaksi==0) {
                                    		echo '<div class="alert alert-success alert-dismissible" style="display:block;">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                                            <h4><i class="icon fa fa-check"></i> Sukses!</h4>
	                                            Input Barang Sukses! Transaksi tidak tercatat.
	                                            </div>';
                                    	} else {
	                                        echo '<div class="alert alert-success alert-dismissible" style="display:block;">
	                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                                            <h4><i class="icon fa fa-check"></i> Sukses!</h4>
	                                            Input Barang Sukses! Transaksi tercatat.
	                                            </div>';
                                    	}
                                    }
                                }
                            ?>
								</div>
                            </div>
                        </div>
                        <div class="clearfix"></div>                                            
                    </div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/data-order.js" type="text/javascript"></script>
<script type="text/javascript">
	var harga = 0;
		
	// $("#barangMasuk").submit(function(e) {
	// 	e.preventDefault();
		
	// 	$("#errorAlert").parent().hide();
	// 	$("#barangResult").empty();

	// 	var id_barang = $("#idbarang").val();
	// 	var id_jenis = $("#jenisbarang").val();
	// 	var ukuran = $("#ukuran").val();
	// 	var foto = $("#foto").val();
	// 	var modalb = parseInt($("#modalb").val());
	// 	var jualb = parseInt($("#jualb").val());
	// 	var id_supplier = $("#supplier").val();
	// 	var jumlah = parseInt($("#jumlah").val());
	// 	var ket = $("#keterangan").val();
				
	// 		$.ajax({
	// 			type: "POST",
	// 			url: "<?= $helper->baseUrl; ?>/api/barang-masuk.php",
	// 			data: 
	// 			{
	// 				barang: id_barang,
	// 				jenis: id_jenis,
	// 				ukuran: ukuran,
	// 				foto:foto,
	// 				modal: modalb,
	// 				jual:jualb,
	// 				supp: id_supplier,
	// 				jumlah:jumlah,
	// 				ket:ket
	// 			},
	// 			success:function(result)
	// 			{
	// 				result = JSON.parse(result);
					
	// 				if(result.success)
	// 				{
	// 					$("#barangResult").html(result.html);
	// 				}
	// 				else
	// 				{
	// 					var errmsg = "Gagal ! ID Barang Sudah Terdaftar.";
	// 					$("#errorAlert").parent().show();
	// 					$("#errorAlert").html(errmsg);
	// 				}
	// 			}
	// 		});
		
		
	// 	document.getElementById("barangMasuk").reset();
	// });

	function hitungHarga()
	{
		var jumlah = parseInt($("#jumlah").val());
		
		if(jumlah > 0)
		{
			var serviceId = $("#orderService").val();
			var service = $.grep(daftarServices, function(e){ return e.id == serviceId; })[0];
			harga = jumlah * (service.rate / 1000);
			
			$("#harga").html(harga);
		}
		else
			$("#harga").html("0");
	}
</script>
