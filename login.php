<?php
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	$pageTitle = "LOGIN";
?>

<!DOCTYPE html>
<html lang="en">
    <?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition login-page">
    <!--<a href="" class="cd-btn cd-modal-trigger">Login</a>-->
        <div class="login-box">
            <div class="login-logo">
                <a href="index.php">
                	<img class="aisen-logo-login" src="assets/img/logo.png" alt="">
                	<!-- <b><?= $helper->title; ?></b> -->
                </a>
            </div>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p id="errorAlert">&nbsp;</p>
			</div>
            <div class="login-box-body">
<!--<center><h2>Maintenance</h2></center><br>-->
                <!-- <p class="login-box-msg">LOGIN</p> -->
                <form role="post" method="post" class="form margin-bottom martop" id="login">
                    <div class="form-group has-feedback">
                        <input name="userid" class="form-control" type="text" id="userid" placeholder="Email atau Username" required />
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input name="password" class="form-control" type="password" id="password" placeholder="Password" required />
                        <span class="fa fa-key form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                        </div>
                    </div>
                </form>
                <!-- register -->
                <!-- <div class="text-center">
                    Need Account ? <a href="http://line.me/ti/p/%40pmp8639n" class="text-center">Register Here</a>
                </div> -->
            </div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<script type="text/javascript">
	$("#login").submit(function(e) {
		e.preventDefault();
		
		$.ajax({
			type: "POST",
			url: "<?= $helper->baseUrl; ?>/api/login.php",
			data: 
			{
				userid: $.trim($("#userid").val()),
				password: $.trim($("#password").val())
			},
			success:function(result)
			{
				result = JSON.parse(result);
				
				if(result.success)
					window.location.href = "index.php";
				else
				{
					$("#errorAlert").parent().show();
					$("#errorAlert").html(result.message);
				}
			}
		});
	});
</script>
