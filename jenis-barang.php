<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Jenis Barang";
	
	if(isset($_GET['act']))
	{
		$sql = "DELETE from jenis_barang where id_jenis = '".$_GET['jenis']."'";
		$resdel = $helper->database->query($sql);
		
	}
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
                    <div class="block">
                        <div class="block">
                            <div class="box">
                            <?php
                            if($resdel==1)
                            {
                            echo '<div class="alert alert-success alert-dismissible" style="display:block;">
                								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                								  <h4><i class="icon fa fa-check"></i> Sukses !</h4>
                								  Jenis Barang Berhasil dihapus.
              									  </div>';
                            }
                            
                            if(isset($_GET['isUpdate']) && isset($_GET['id']))
                            {   
                                $isUpdate = $_GET['isUpdate'];
                                if ($isUpdate==1) {
                                    $idUpdated = $_GET['id'];
                                    echo '<div class="box-header">
                                    <div class="alert alert-success alert-dismissible" style="display:block;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> Sukses !</h4>
                                    Jenis Barang '.$idUpdated.' Berhasil diupdate.
                                    </div>
                                    </div>';
                                } else {
                                    $idUpdated = $_GET['id'];
                                    echo '<div class="box-header">
                                    <div class="alert alert-danger alert-dismissible" style="display:block;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-close"></i> Gagal!</h4>
                                    Jenis Barang '.$idUpdated.' Gagal diupdate.
                                    </div>
                                    </div>';
                                }
                            }
                            ?>
                                <div class="box-header">
                                    <h3 class="box-title">Tabel Jenis Barang</h3>
                                    <div class="pull-right"><a href="add-jenis.php" class="btn btn-primary">Tambah Jenis Barang</a></div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                    <!-- <th class="rata-tengah">ID</th> -->
                                                    <th class="rata-tengah">Nama Barang</th>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                    <th class="rata-tengah">Nama Barang</th>
                                                    <?php endif; ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
													$list = $helper->database->select("jenis_barang", "*", "1");
													
													foreach($list as $service)
													{
														$html = "<tr>";
                                                        // $html .= "<td align='center'>".$service->id_jenis."</td>";
														$html .= "<td align='center'>".$service->jenis_barang."</td>";
														// $html .= "<td>".$service->alamat."</td>";
														// $html .= "<td>".$service->telepone."</td>";
                                                        if($_SESSION['level'] == 'Admin'):
                                                        $html .= "<td align='center'><a style='margin-right:10px;' href=\"".$helper->baseUrl."/edit-jenis.php?id_jenis=$service->id_jenis\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$service->id."' >
                                                                    <i class='fa fa-hand-pointer-o'> Ubah</i>
                                                                </button></a><a href=\"".$helper->baseUrl."/jenis-barang.php?act=delete&jenis=$service->id_jenis\"<button type='button' class='btn btn-danger ' data-order-id='".$service->id."' >
                                                                    <i class='fa fa-close'> Hapus</i>
                                                                </button></a></td>";
                                                        endif;
                                                                
														$html .= "</tr>";
														
														echo $html;
													}
												?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
