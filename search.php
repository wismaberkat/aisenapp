<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Search";
?>

<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Cari Barang</h1>
                </section>
                <section class="content">
                	<div class="row">
                		<div class="col-xs-12 col-sm-12">
                			<div class="form-group">
                				<label class="control-label" for="kategori">Jenis Barang :</label>
                				<select id="kategori" class="form-control select2" required onchange="showJenis(this.value)">
                    				<option value="">-- Pilih Jenis Barang --</option>
                    					<?php 
                    					$sql_jenis= "SELECT * FROM jenis_barang";
                    					$res_jenis = $helper->database->query($sql_jenis);
                    					if (!$res_jenis) {
                    						echo "Jenis tidak ada";
                    					}else{
                    						if ($res_jenis->num_rows > 0) {
                    							while ($jenis = $res_jenis->fetch_assoc()) {
                    								$id_jenis = $jenis['id_jenis'];
                    								$jenis_barang = $jenis['jenis_barang'];
                    								?>
                    								<option value="<?php echo $id_jenis; ?>"><?php echo $jenis_barang; ?></option>
                    								<?php 
												}//end of while $jenis
												$res_jenis->data_seek(0);
											}//endif $res_jenis
										}//end of else 
										?>
								</select>
							</div>
							<div class="margin-bottom hidden-xs"></div>
						</div>
					</div>
                	<div class="row">
                		<div class="col-xs-12 col-sm-12">
                			<div id="txtHint">Hasil pencarian akan muncul setelah jenis di pilih.</div>
                		</div>
                	</div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
        
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/data-order.js" type="text/javascript"></script>
<script>
function showJenis(str) {
    try {

    var xhttp;
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("txtHint").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "api/jenis.php?jenis="+str, true);
    xhttp.send();
    } catch (e) {
        console.log("something went wrong !.");
    }
}
</script>