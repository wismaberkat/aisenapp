<?php
    session_start();
	
    if(!isset($_SESSION["username"]) || $_SESSION['level'] != 'Admin')
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Edit History Masuk";
	
    $username = $_SESSION["username"];
    
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Form Edit History Masuk</h3>
								</div>
								<?php 
									$id = $_GET['id'];
								?>
								<form method="post" action="<?php echo "edit-m.php?id_masuk=".$id; ?>">
									<div class="box-body">
									<?php
									// $id = $_GET['id_masuk'];
									// echo $id;
									$sql = "SELECT * FROM history_masuk where id_masuk='$id'";
    								$res = $helper->database->query($sql);
    								if (!$res) {
    									// echo "Supplier Tidak";
    								} else {
    									$history = $res->fetch_assoc();
    									// $id = $history['id_masuk'];
    									// $barang = $history['id_barang'];
    									// $jenis = $history['id_jenis'];
    									// $ukuran = $history['ukuran'];
    									// $supp = $history['id_supplier'];
    									// $tanggal_masuk = $history['tanggal_masuk'];
    									// $jumlah_masuk = $history['jumlah'];
    									// $keterangan = $history['keterangan'];
    									// $submit_by = $history['uplink'];
    									// echo $ids;
    								}
								?>
										<div class="row">
											<div class="col-md-12">
													<input type="hidden" name="id_masuk_awal" value="<?=$id;?>">
													<input type="hidden" name="id_masuk" class="form-control" id="id_masuk" value="<?=$history['id_masuk']?>" readonly>
												<div class="form-group">
													<label class="control-label" for="id_barang">Kode Barang</label>
													<input type="text" name="id_barang" class="form-control" id="id_barang"  value="<?=$history['id_barang']?>"  />
													<input type="hidden" name="id_barang_lama" class="form-control" id="id_barang"  value="<?=$history['id_barang']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="jenis_barang">Jenis</label>
													<select name="id_jenis" class="form-control" readonly>
														<?php 
														$id_jen = $history['id_jenis'];
														$sql_jenis= "SELECT * FROM jenis_barang";
														$res_jenis = $helper->database->query($sql_jenis);
														if (!$res_jenis) {
															echo "Jenis tidak ada";
														}else{
															if ($res_jenis->num_rows > 0) {
																while ($jenis = $res_jenis->fetch_assoc()) {
																	$id_jenis = $jenis['id_jenis'];
																	$jenis_barang = $jenis['jenis_barang'];
														?>
																	<option value="<?php echo $id_jenis; ?>"<?php if($id_jen==$id_jenis){echo "selected";}else{} ?>><?php echo $jenis_barang; ?></option>
														<?php 
																}//end of while $jenis
																$res_jenis->data_seek(0);
																foreach ($jenis as $jenisnya) {
																		echo $jenisnya['jenis_barang'];
																	}
															}//endif $res_jenis
														}//end of else 
														?>
													</select>
												</div>
												<div class="form-group">
													<label class="control-label" for="ukuran">Ukuran</label>
													<input type="text" name="ukuran" class="form-control" id="ukuran"  value="<?=$history['ukuran']?>"  readonly/>
												</div>
												<div class="form-group">
													<label class="control-label" for="level">Supplier</label>
													<select name="id_supplier" class="form-control" readonly>
														<option value="">-- Pilih Supplier --</option>
														<?php 
															$id_supp = $history['id_supplier'];
															$sql_supplier= "SELECT * FROM supplier";
															$res_supplier = $helper->database->query($sql_supplier);
															if (!$res_supplier) {
																echo "Supplier Tidak ada";
															}else{
																if ($res_supplier->num_rows > 0) {
																	while ($supplier = $res_supplier->fetch_assoc()) {
																		$id_supplier = $supplier['id_supplier'];
																		$nama_supplier = $supplier['nama_supplier'];
														?>
																		<option value="<?php echo $id_supplier; ?>"<?php if($id_supp==$id_supplier){echo "selected";}else{} ?>><?php echo $nama_supplier; ?></option>
														<?php 
																	}//end of while $res_supplier
																	$res_supplier->data_seek(0);
																}//endif $res_supplier
															}//end of else 
														?>
													</select>
												</div>
												<div class="form-group">
													<label class="control-label" for="tanggal_masuk">Tanggal Masuk</label>
													<input type="text" name="tanggal_masuk" class="form-control" id="tanggal_masuk"  value="<?=$history['tanggal_masuk']?>" readonly />
												</div>
												<div class="form-group">
													<label class="control-label" for="jumlah">Jumlah Masuk</label>
													<input type="number" min="0" name="jumlah" class="form-control" id="jumlah"  value="<?=$history['jumlah']?>"  />
												</div>
												<div class="form-group">
													<label class="control-label" for="keterangan">Keterangan</label>
													<textarea name="keterangan" id="keterangan" class="form-control" cols="20" rows="3"><?=$history['keterangan']?></textarea>
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right"> Update </button>
												</div> 
											</div>
										</div>
									</div>
								</form>								
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>