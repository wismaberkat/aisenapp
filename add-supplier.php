<?php
    session_start();
	
    if(!isset($_SESSION["username"]) || $_SESSION['level'] != 'Admin'){
    	session_destroy();
        header("location:login.php");
    }
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
	$pageTitle = "Tambah Supplier";
	
    $username = $_SESSION["username"];
    
    //var_dump($servers); 
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
			<div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?></h1>
                </section>
                <section class="content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Tambah Supplier</h3>
								</div>
								<form method="post" action="">
									<div class="box-body">
									<?php
									
									if(isset($_POST['nama_supplier'])):
										
										$id = trim($_POST['id_supplier']);
										$nama = trim($_POST['nama_supplier']);
										$alamat = trim($_POST['alamat']);
										$telepone = trim($_POST['telepone']);

										$sql = "INSERT INTO supplier(nama_supplier,alamat,telepone) VALUES('$nama','$alamat','$telepone')";
										$res = $helper->database->query($sql);

									if($res == 1)
									{
										echo '<div class="alert alert-success alert-dismissible" style="display:block;">
                								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                								  <h4><i class="icon fa fa-check"></i> Alert!</h4>
                								  Supplier Berhasil Ditambahkan.
              									  </div>';
									}else{
										echo '<div class="alert alert-danger alert-dismissible" style="display:block;">
                								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                								  <h4><i class="icon fa fa-close"></i> Alert!</h4>
                								  Gagal Menambahkan Supplier.
              									  </div>';

									}
									endif;
									?>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="prod_name">Nama Supplier</label>
													<input type="text" name="nama_supplier" class="form-control" id="nama_supplier" placeholder="Masukkan Nama Supplier" value="" required/>
												</div>
												<div class="form-group">
													<label class="control-label" for="alamat">Alamat</label>
													<input type="text" name="alamat" class="form-control" id="alamat" placeholder="Masukkan Alamat Supplier"value="" />
												</div>
												<div class="form-group">
													<label class="control-label" for="telepone">Telepone</label>
													<input type="text" name="telepone" class="form-control" id="telepone" placeholder="Masukkan Telepon Supplier"value="" />
												</div>
												<div class="box-footer">
													<button type="submit" class="btn btn-info pull-right">Add Supplier</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
				</section>
			</div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
