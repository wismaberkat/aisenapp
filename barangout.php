<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "Barang Keluar";

    
?>

<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?> <small>Masukkan detail barang yang keluar.</small></h1>
                </section>
                <section class="content">
                    <div class="row">
                    	<div class="block">
                            <div class="col-xs-12">
                        	<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tabel Daftar Barang</h3>
                                    <div class="col-md-4" style="float:right;">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Masukkan Kode Barang/Jenis Barang">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Search</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                	<th class="rata-tengah">NO</th>
                                                	<th class="rata-tengah">Kode Barang</th>
                                                	<th class="rata-tengah">Jenis Barang</th>
                                                	<th class="rata-tengah">Ukuran</th>
                                                	<th class="rata-tengah">Foto</th>
                                                    <?php if($_SESSION['level'] == 'Admin'): ?>
                                                	<th class="rata-tengah">BP</th>
                                                    <?php endif; ?>
                                                	<th class="rata-tengah">Harga Jual</th>
                                                	<th class="rata-tengah">Supplier</th>
                                                	<th class="rata-tengah">Keterangan</th>
                                                	<th class="rata-tengah">Stok</th>
                                                	
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php
    													$i = 1;
                                                        $barang = $helper->database->select("daftar_barang", "*","1");											
    													foreach($barang as $stock_barang)
    													{
                                                            $jumlah_awal = $stock_barang->jumlah;
                                                            // echo $jumlah_awal;
                                                            //get jenis_barang
                                                            $id_jenis = $stock_barang->id_jenis;
                                                            $j_condition = "id_jenis='$id_jenis'";
                                                            $jenis = $helper->database->select("jenis_barang", "jenis_barang",$j_condition);
                                                            // echo $jenis;

                                                            $html = "<tr>";
                                                            $html .= "<td align='center'>".$i."</td>";
                                                            $html .= "<td align='center'>".$stock_barang->id_barang."</td>";

                                                            foreach($jenis as $jenis_barang)
                                                            {
                                                                $html .= "<td align='center'>".$jenis_barang->jenis_barang."</td>";
                                                            }
                                                            $html .= "<td align='center'>".$stock_barang->ukuran."</td>";
    														$html .= "<td align='center'>".$stock_barang->foto."</td>";
                                                            if($_SESSION['level'] == 'Admin'):
                                                            $html .= "<td align='right'>".$helper->format_rupiah($stock_barang->h_modal)."</td>";
                                                            endif;
                                                            $html .= "<td align='right'>".$helper->format_rupiah($stock_barang->h_jual)."</td>";
                                                            //get nama_supplier
                                                            $id_supplier = $stock_barang->id_supplier;
                                                            $s_condition = "id_supplier='$id_supplier'";
                                                            $supp = $helper->database->select("supplier", "nama_supplier",$s_condition);
                                                            foreach($supp as $supplier)
                                                            {
                                                                $html .= "<td align='center'>".$supplier->nama_supplier."</td>";
                                                            }
                                                            $html .= "<td align='center'>".$stock_barang->keterangan."</td>";
                                                            $html .= "<td align='right'>".$stock_barang->jumlah."</td>";
                                                            // $html .= "<td><a href=\"".$helper->baseUrl."/edit-barang.php?id_barang=$stock_barang->id_barang\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$user->id."' >
                                                            //             <i class='fa fa-hand-pointer-o'></i>
                                                            //         </button></a></td>";
    														$html .= "</tr>";
    														$html .='<input type="hidden" id="jumlah_awal" value="'.$stock_barang->jumlah.'">';
    														echo $html;
    														
    														$i++;
    													}
    												?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="block">
                			<div class="col-xs-12 col-sm-7">
                				<div class="box box-default">
                					<div class="box-header with-border">
                                        <h3 class="box-title">Form Barang Keluar</h3>
                					</div>
                                    <form action="api/barang-keluar.php" method="post">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="form-group">
                                                                <label class="control-label" for="id_barang">Kode Barang</label>
                                                                <input type="text" class="form-control" name="id_barang" value="" autocomplete="off" placeholder="Kode Barang" onkeyup="showHint(this.value)" required>
                                                            </div>
                                                            <div class="margin-bottom hidden-xs"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="form-group">
                                                                <label class="control-label" for="keluar">Jumlah Keluar</label>
                                                                <input class="form-control" name="jumlah_keluar" id="keluar" onkeyup="hitungStock();" placeholder="Jumlah Barang Keluar" type="number" required >
                                                            </div>
                                                            <div class="margin-bottom hidden-xs"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="form-group">
                                                                <label class="control-label" for="kirim">Tanggal Kirim</label>
                                                                <input class="form-control" name="tanggal_kirim" id="tanggal_kirim" type="text" placeholder="Tanggal Kirim" required />
                                                            </div>
                                                            <div class="margin-bottom hidden-xs"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="form-group">
                                                                <label class="control-label" for="supplier">Keterangan :</label>
                                                                <textarea name="keterangan_keluar" id="keterangan" class="form-control" cols="20" rows="3" placeholder="Masukkan Keterangan"></textarea>
                                                            </div>
                                                            <div class="margin-bottom hidden-xs"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <!-- <a href="<?php $helper->baseUrl.'/barang-keluar.php?' ?>"> -->
                                                            <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                                                            <!-- </a> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                				</div>
                			</div>
                            <div class="col-sm-5">
                                <?php 

                                    if (isset($_GET["id"]) && isset($_GET["keluar"])) {
                                        $sukses = $_GET["keluar"];
                                        $idkeluar = $_GET["id"];
                                        if ($sukses==0) {
                                            echo '<div class="alert alert-danger alert-dismissible" style="display:block;">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <h4><i class="icon fa fa-close"></i> Gagal!</h4>'
                                                .$_GET["notif"].
                                                '</div>';
                                                
                                        } else {
                                            echo '<div class="alert alert-success alert-dismissible" style="display:block;">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <h4><i class="icon fa fa-check"></i> Sukses!</h4>'
                                                .$_GET["notif"].
                                                '</div>';
                                        }
                                    }
                                ?>
                            </div>
                		</div>
                	</div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
    </body>
</html>
<?php include_once "views/templates/scripts.php"; ?>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/data-order.js" type="text/javascript"></script>
<script src="<?= $helper->baseUrl; ?>/assets/site/js/jquery-ui.js"></script>
<script>
var dateToday = new Date();
var dates = $("#tanggal_kirim").datepicker({
    defaultDate: "+1d",
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
    }
});
</script>
