<?php
session_start();

if(!isset($_SESSION["username"]) || $_SESSION['level'] != 'Admin')
	header("location:login.php");

require_once "api/Helpers/GlobalHelper.php";

$helper = new GlobalHelper();

$pageTitle = "My Account";

$username = $_SESSION["username"];
$sql = "SELECT * FROM user where id=".$_GET['user'];
$res = $helper->database->query($sql);
$user = $res->fetch_assoc();
if (isset($_POST['level'])) {
	$data = array(
		array("level", $_POST['level'])
		);

	$reslevel = $helper->database->update("user", $data, "id = '".$user['id']."'");
}


$sql = "SELECT * FROM user where id=".$_GET['user'];
$res = $helper->database->query($sql);
$user = $res->fetch_assoc();


?>
<!DOCTYPE html>
<html lang="en">
<?php include_once "views/templates/head.php"; ?>
<body class="hold-transition skin-blue sidebar-mini loading">
	<div class="wrapper">
		<?php include_once "views/templates/header.php"; ?>
		<aside class="main-sidebar">
			<?php include_once "views/templates/section_menu.php"; ?>
		</aside>
		<div class="content-wrapper">
			<section class="content-header">
				<h1><?= $pageTitle; ?> <small>Control panel</small></h1>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Ganti password</h3>
							</div>
							<form method="post" action="">
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label" for="current_password">Password Lama</label>
												<input type="password" name="passlama" class="form-control" id="current_password" value="" required/>
											</div>
											<div class="form-group">
												<label class="control-label" for="new_password">Password Baru</label>
												<input type="password" name="passbaru1" class="form-control" id="new_password" required/>
											</div>
											<div class="form-group">
												<label class="control-label" for="confirm_password">Konfirmasi Password Baru</label>
												<input type="password" name="passbaru2" class="form-control" id="confirm_password" required/>
											</div>
											<div class="box-footer">
												<button type="submit" class="btn btn-info pull-right">Change Password</button>
											</div> 
										</div>
									</div>
								</div>
							</form>
							<form method="post" action="">
								<div class="box-header with-border">
									<h3 class="box-title">Update Level</h3>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label" for="level">Level</label>
												<select name="level" class="form-control">
													<option value="Admin" <?=$user['level'] == 'Admin' ? 'selected' : ''?>>Admin</option>
													<option value="Karyawan" <?=$user['level'] == 'Karyawan' ? 'selected' : ''?>>Karyawan</option>
												</select>
											</div>
											<div class="box-footer">
												<button type="submit" class="btn btn-info pull-right">Update</button>
											</div> 
										</div>
									</div>
								</div>
							</form>
						</div>
							<?php 
							//validasi level
							if(isset($reslevel))
							{
								echo '<div class="alert alert-success alert-dismissible" style="display:block;">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-check"></i> Alert!</h4>
								Data Berhasil diupdate.
								</div>';
							}
							?>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="box box-default">
							<form action="" method="post">
								<div class="box-header with-border">
									<h3 class="box-title">My Profile</h3>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label" for="Username">Username</label>
												<input type="text" class="form-control" value="<?= $user["username"]; ?>" readonly>
											</div>
											<div class="form-group">
												<label class="control-label" for="Nama">Nama</label>
												<input type="text" class="form-control" value="<?= $user["nama"]; ?>" readonly>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					<?php
						//validasi password
						if (isset($_POST['passlama']))
						{
							$password = $helper->database->select("user", "password", "username = '$username'")[0]->password;

							$passlama = $_POST["passlama"];
							$passbaru1 = $_POST["passbaru1"];
							$passbaru2 = $_POST["passbaru2"];

							if(!$passlama || !$passbaru1 || !$passbaru2){
								echo '<div class="alert alert-warning alert-dismissible" style="display:block;">
											  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											  <h4><i class="icon fa fa-close"></i> Gagal !</h4>
											  Data Kurang!.
												  </div>';
								//exit();
							}
							else if($password != $passlama){
								echo '<div class="alert alert-warning alert-dismissible" style="display:block;">
											  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											  <h4><i class="icon fa fa-close"></i> Gagal !</h4>
											  Password lama salah
												  </div>';
								//exit();
							} else {
								$data = array(
									array("password", $passbaru1)
									);

								$result = $helper->database->update("user", $data, "username = '$username'");

								if($result){
									echo '<div class="alert alert-success alert-dismissible" style="display:block;">
												  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												  <h4><i class="icon fa fa-check"></i> Sukses!</h4>
												  Password Berhasil Diubah.
													  </div>';
								} else {
									echo '<div class="alert alert-danger alert-dismissible" style="display:block;">
												  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												  <h4><i class="icon fa fa-close"></i> Gagal!</h4>
												  Ada kesalahan saat penggantian password.
													  </div>';
								}
							}

						}
					?>
					</div>
				</div>
			</section>
		</div>
		<?php include_once "views/templates/footer.php"; ?>
		<div class="control-sidebar-bg">
		</div>
	</div>
</body>
</html>
<?php include_once "views/templates/scripts.php"; ?>