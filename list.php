<?php
    session_start();
	
    if(!isset($_SESSION["username"]))
        header("location:login.php");
	
	require_once "api/Helpers/GlobalHelper.php";
	
	$helper = new GlobalHelper();
	
    $username = $_SESSION["username"];
	$pageTitle = "List Harga";
	
	if(isset($_GET['act']) && $_SESSION['level'] == 'Admin')
	{
		$sql = "DELETE from list_product where id = '".$_GET['product']."'";
		$resdel = $helper->database->query($sql);
		
	}
?>
<!DOCTYPE html>
<html lang="en">
	<?php include_once "views/templates/head.php"; ?>
    <body class="hold-transition skin-blue sidebar-mini loading">
        <div class="wrapper">
            <?php include_once "views/templates/header.php"; ?>
            <aside class="main-sidebar">
				<?php include_once "views/templates/section_menu.php"; ?>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= $pageTitle; ?> <small>Control panel</small></h1>
                </section>
                <section class="content">
                    <div class="margin-bottom no-print">
                        <div style="margin-bottom: 0!important;" class="callout callout-danger">
                            <h4><i class="fa fa-exclamation-circle"></i> Attention!</h4>
                             Harga yang tertera disini adalah <strong>Harga per 1000 quantity</strong> (harga per 100 adalah 10% dari harga dibawah). Saat melakukan order harap mengisi format dengan benar dan jumlah minimal &amp; maximal order diperhatikan (Panel ini bekerja otomatis sehingga tidak bisa direvisi oleh Admin jika ada kesalahan input). Harap membaca tab 'Frequently Asked Question' terlebih dahulu, Terimakasih.
                        </div>
                    </div>
                    <div class="block">
                        <div class="block">
                            <div class="box">
                            <?php
                            if($resdel==1)
                            {
                            echo '<div class="alert alert-success alert-dismissible" style="display:block;">
                								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                								  <h4><i class="icon fa fa-check"></i> Alert!</h4>
                								  Data Produk Berhasil dihapus.
              									  </div>';
                            }
                            ?>
                                <div class="box-header">
                                    <h3 class="box-title">Tabel List Harga</h3>
                                    <?php if($_SESSION['level'] == 'Admin'):?>
                                    <div class="pull-right"><a href="add-product.php" class="btn btn-primary">Tambah</a></div>
                                    <?php endif;?>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-hover table-striped table-font-size">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Layanan</th>
                                                    <th>Rate (Per 1000)</th>
                                                    <th>Min Order</th>
                                                    <th>Max Order</th>
                                                    <th>Status</th>
                                                    <?php
                                                    if($_SESSION['level']=='Admin'):
                                                    echo '<th>Action</th>';
                                                    endif;
                                                    ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
													$list = $helper->database->select("list_product", "*", "1");
													
													foreach($list as $service)
													{
														$html = "<tr>";
                                                        $html .= "<td>".$service->id."</td>";
														$html .= "<td>".$service->server."</td>";
														$html .= "<td>".$service->rate."</td>";
														$html .= "<td>".$service->min."</td>";
														$html .= "<td>".$service->max."</td>";
														$html .= "<td>".$service->status."</td>";
                                                        if($_SESSION['level'] == 'Admin'):
                                                        $html .= "<td><a style='margin-right:10px;' href=\"".$helper->baseUrl."/editproduct.php?product=$service->id\"<button type='button' class='btn btn-warning progressBtn' data-order-id='".$service->id."' >
                                                                    <i class='fa fa-hand-pointer-o'></i>
                                                                </button></a><a href=\"".$helper->baseUrl."/list.php?act=delete&product=$service->id\"<button type='button' class='btn btn-danger ' data-order-id='".$service->id."' >
                                                                    <i class='fa fa-close'></i>
                                                                </button></a></td>";
                                                                
                                                        endif;
														$html .= "</tr>";
														
														echo $html;
													}
												?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include_once "views/templates/footer.php"; ?>
            <div class="control-sidebar-bg"></div>
        </div>
        <?php include_once "views/templates/scripts.php"; ?>
    </body>
</html>
